#!/usr/bin/python
import sys
import os
import numpy as np
import scipy
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import random
import sys
import scipy
import scipy.stats
from scipy.interpolate import interp1d
from sklearn.decomposition import PCA
from sklearn.utils.extmath import randomized_svd
from sklearn.manifold import TSNE
from sklearn.metrics import auc
from scipy.stats import pearsonr
import os
sys.setrecursionlimit(10000)
from sklearn import svm
from sklearn.svm import SVR
from sklearn.metrics import r2_score
from sklearn.calibration import CalibratedClassifierCV
from sklearn.linear_model import Lasso
from sklearn.linear_model import Ridge
from sklearn.linear_model import ElasticNet
import load_seqfishplus_new, load_brain1_new, load_brain2_new
import reader
import umap
import scanpy as sc

def shuffle(df, n=1, axis=0):
	df = df.copy()
	for _ in range(n):
		df.apply(np.random.shuffle, axis=axis)
	return df

def get_rev_clust_annot(clust_annot):
	rev_clust_annot = {}
	for ca in clust_annot:
		rev_clust_annot[clust_annot[ca]] = ca
	return rev_clust_annot

def read_from_file_mode(mode="nissl", brain=3): #or dapi #annot is cell type or domain or morphology
	workdir="/home/qzhu/Downloads/morphology.dec6"
	file_prefix_2="%s/feature.umap.seqfishplus.mat3_2.h5ad" % workdir #nissl
	file_prefix="mat3_2"
	if mode=="dapi":
		file_prefix_2="%s/feature.umap.seqfishplus.mat3_1.h5ad" % workdir #dapi
		file_prefix="mat3_1"
	adata = sc.read_h5ad(file_prefix_2)
	px_int = pd.read_csv("%s/mat3_2.leiden.2.5.csv" % workdir, index_col=0, dtype="int32").to_numpy().flatten()
	t_max = np.max(px_int)
	print(px_int.shape)
	cell_clust = []
	if brain==3:
		f_name = "%s/seqfishplus/adata.brain.seqfishplus.use_equalize.True.distance.minkowski.h5ad" % workdir
	elif brain==1:
		f_name = "%s/adata.brain.1.option.1.distance.minkowski.h5ad" % workdir
	elif brain==2:
		f_name = "%s/adata.brain.2.option.1.distance.minkowski.h5ad" % workdir
	s_mat = sc.read_h5ad(f_name)
	cell_clust = s_mat.obs["leiden_2.0"].values.astype(int)
	c_max = np.max(cell_clust)
	X = adata.X
	sizes = [1401, 1589, 523]
	if brain==3:
		offset = 1401+1589
		X = adata.X[:, offset:]
	elif brain==1:
		offset = 0
		X = adata.X[:, offset:offset+1401]
	elif brain==2:
		offset = 1401
		X = adata.X[:, offset:offset+1589]
	return X


if __name__=="__main__":
	t_brain = 1
	t_option = 1
	t_distance = "minkowski"
	mat2, ct_reorder, ct2_reorder, domain_reorder, Xcell_reorder, num_cell = None, None, None, None, None, 0
	if t_brain==1:
		mat2, ct_reorder, ct2_reorder, domain_reorder, Xcell_reorder, num_cell = load_brain1_new.load(filter_feature=False, log=False, option=t_option)
	else:
		mat2, ct_reorder, ct2_reorder, domain_reorder, Xcell_reorder, num_cell = load_brain2_new.load(filter_feature=False, log=False, option=t_option)
	#mat2, ct_reorder, domain_reorder, Xcell_reorder, num_cell = load_seqfishplus_new.load(filter_feature=False, log=False, use_equalize=True)
	#nissl feature aggregations	
	px_int = pd.read_csv("/home/qzhu/Downloads/morphology.dec6/mat3_2.leiden.2.5.csv", index_col=0, dtype="int32").to_numpy().flatten()
	t_max = np.max(px_int)
	#dapi feature aggregations
	px_int2 = pd.read_csv("/home/qzhu/Downloads/morphology.dec6/mat3_1.leiden.2.5.csv", index_col=0, dtype="int32").to_numpy().flatten()
	u_max = np.max(px_int2)
	print(t_max, u_max, "Done")

	t_use_equalize=True
	binary_file = "adata.brain.%d.option.%d.distance.%s.feature.agg.h5ad" % (t_brain, t_option, t_distance)
	adata = sc.read_h5ad(binary_file)
	unsuper = adata.obs["leiden_2.0"].values.astype(int)
	print(unsuper)

	#aggregations
	agg_mat = np.empty((mat2.shape[0], (t_max + 1 + u_max+1)), dtype="float32")
	cur=0
	#dapi first
	for i in range(u_max+1):
		t_ids = np.where(px_int2==i)[0]
		Y = np.zeros((px_int2.shape[0]), dtype="int32")
		Y[t_ids] = 1
		X = np.transpose(mat2[:, 0:4096])
		clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, verbose=0, max_iter=10000, tol=1e-4, random_state=100)
		clf.fit(X, Y)
		coef = clf.coef_
		#z = coef
		z = 1/(1+np.exp(-coef))
		agg_mat[:,cur] = z
		cur+=1			
		print(i, "Done1")
	#nissl next
	for i in range(t_max+1):
		t_ids = np.where(px_int==i)[0]
		Y = np.zeros((px_int.shape[0]), dtype="int32")
		Y[t_ids] = 1
		X = np.transpose(mat2[:, 4096:2*4096])
		clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, verbose=0, max_iter=10000, tol=1e-4, random_state=100)
		clf.fit(X, Y)
		coef = clf.coef_
		z = 1/(1+np.exp(-coef))
		#z = coef
		agg_mat[:, cur] = z
		cur+=1
		print(i, "Done2")
	mat2 = agg_mat
	#=======================================

	clust_id = domain_reorder
	original = clust_id.copy()
	#clust_id = clust_id - 1
	#clust_id = to_categorical(clust_id)
	#plt.scatter(X_emb[:,0], X_emb[:,1], s=20, cmap="tab20b", c=clust_id, alpha=1.0)
	#plt.show()
	#mat2 = mat2[:, 4096:] #limit to nissl only

	#do_item====================
	do_item = "cell_type"
	#===========================
	if do_item=="domain":
		reorder = domain_reorder
	else:
		reorder = ct_reorder
	num_class = np.unique(reorder).shape[0]

	print(mat2.shape)
	#coef_1 = np.empty((num_class*num_times, mat2.shape[1]), dtype="float32")
	X = mat2
	Y = reorder
	clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e3, verbose=0, max_iter=10000, tol=1e-4)
	clf.fit(X,Y)
	coef_1 = clf.coef_
	coef_DAPI = coef_1[:, 0:24]
	coef_Nissl = coef_1[:, 24:]

	matplotlib.rcParams.update({"font.size": 16})
	
	num_class_unsuper = np.unique(unsuper).shape[0]
	w_unsuper = np.empty((num_class_unsuper, 24+23), dtype="float32")
	for i in range(num_class_unsuper):
		n1 = np.where(unsuper==i)[0]
		w_unsuper[i,:] = np.mean(mat2[n1, :], axis=0)
	row_order = ["%d" % n for n in range(num_class_unsuper)]
	nt = {}
	for ki in range(24):
		nt["D%d" % ki] = pd.Series(w_unsuper[:, ki], index=row_order)
	for ki in range(23):
		nt["N%d" % ki] = pd.Series(w_unsuper[:, 24+ki], index=row_order)

	g = sns.clustermap(nt, cmap="plasma", figsize=(16,7.5), dendrogram_ratio=(0.05,0.1), cbar_pos=(0.02, 0.85, 0.02, 0.12))
	plt.setp(g.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
	plt.setp(g.ax_heatmap.xaxis.get_majorticklabels(), rotation=90)
	g.savefig("metacor.map.brain.%d.both.nissl.dapi.png" % t_brain, dpi=300)
	plt.show()
	
	weight = np.empty((num_class, 24+23), dtype="float32")
	for i in range(num_class):
		n1 = np.where(reorder==i+1)[0]
		weight[i,:] = np.mean(mat2[n1, :], axis=0)

	weight_D = weight[:, 0:24]
	weight_N = weight[:, 24:]

	weight_N = weight_N * coef_Nissl
	weight_D = weight_D * coef_DAPI

	clust_annot = {1:"Astro", 2:"Endo", 3:"L2/3 glut", 4:"L4 glut", 5:"L5 glut", 6:"L6 glut", 7:"Micro", 8:"OPC", \
	9:"Oligo", 10:"Pvalb_1", 11:"Pvalb_2", 12:"Sst", 13:"Vip"}
	rev_clust_annot = get_rev_clust_annot(clust_annot)
	row_order = [rev_clust_annot[x] for x in ["Astro", "Endo", "Micro", "OPC", "Oligo", "Pvalb_1", "L2/3 glut", "L4 glut", "Pvalb_2", "L5 glut", "L6 glut", "Vip", "Sst"]]

	
	if do_item=="domain" and t_brain==1:
		clust_annot = {1:"L5", 2:"L4", 3:"L2/3", 4:"L1.u,L6b.u", 5:"L1.l,L6b.l", 6:"L6b", 7:"L6a", 8:"L5,L6a", 9:"L4.2"}
		rev_clust_annot = get_rev_clust_annot(clust_annot)
		row_order = [rev_clust_annot[x] for x in ["L1.u,L6b.u", "L1.l,L6b.l", "L2/3","L4", "L5","L5,L6a","L6a", "L6b", "L4.2"]]

	elif do_item=="domain" and t_brain==2:
		clust_annot = {1:"O2", 2:"I1a", 3:"O4", 4:"I1b", 5:"O1", 6:"I2", 7:"I3", 8:"O3", 9:"IS"}
		rev_clust_annot = get_rev_clust_annot(clust_annot)
		row_order = [rev_clust_annot[x] for x in ["O1","O4","O3","O2","I1a","I1b","I2","I3", "IS"]]

	elif do_item=="cell_type" and t_brain==1:
		clust_annot = {1:"Astro", 2:"Neuron", 3:"Oligo", 4:"Endo", 5:"Micro"}
		rev_clust_annot = get_rev_clust_annot(clust_annot)
		row_order = [rev_clust_annot[x] for x in ["Endo", "Astro", "Micro", "Oligo", "Neuron"]]

	elif do_item=="cell_type" and t_brain==2:
		clust_annot = {1:"Astro", 2:"Neuron", 3:"Oligo", 4:"Endo", 5:"Micro"}
		rev_clust_annot = get_rev_clust_annot(clust_annot)
		row_order = [rev_clust_annot[x] for x in ["Endo", "Astro", "Micro", "Oligo", "Neuron"]]

	row_order_title=["%s" % clust_annot[r] for r in row_order]
	nt = {}
	print("coef_dapi", coef_DAPI.shape)
	print("coef_nissl", coef_Nissl.shape)
	for ki in range(24):
		t_mode = "D"
		nx = [coef_DAPI[r-1,ki] for r in row_order]
		nt["%s%d" % (t_mode, ki)] = pd.Series(nx, index=row_order_title)
	for ki in range(23):	
		t_mode = "N"
		nx = [coef_Nissl[r-1, ki] for r in row_order]
		nt["%s%d" % (t_mode, ki)] = pd.Series(nx, index=row_order_title)

	g = sns.clustermap(nt, cmap="plasma", figsize=(16,5), dendrogram_ratio=(0.05,0.1), cbar_pos=(0.02, 0.85, 0.02, 0.12))
	g.savefig("metasvmweight.map.brain.%d.%s.both.nissl.dapi.png" % (t_brain, do_item), dpi=300)
	plt.show()
	
	print(coef_1.shape)

	sys.exit(0)
	for i in range(num_class):
		n1 = np.where(reorder==i+1)[0]
		num_train = int(n1.shape[0] * ratio)
		c_size.append(num_train)
		this_train_ex = np.random.choice(n1, num_train, replace=False)
		this_test_ex = np.setdiff1d(n1, this_train_ex)
		train_ex.extend(this_train_ex)
		test_ex.extend(this_test_ex)

	print(num_class)
	for nt in range(num_times):
		print("Time", nt)
		c_size = []
		train_ex = []
		test_ex = []
		for i in range(num_class):
			n1 = np.where(reorder==i+1)[0]
			num_train = int(n1.shape[0] * ratio)
			c_size.append(num_train)
			this_train_ex = np.random.choice(n1, num_train, replace=False)
			this_test_ex = np.setdiff1d(n1, this_train_ex)
			train_ex.extend(this_train_ex)
			test_ex.extend(this_test_ex)
		train_ex = np.array(train_ex)
		test_ex = np.array(test_ex)
		X = mat2[train_ex, :]
		X_test = mat2[test_ex, :]
		Y = reorder[train_ex]
		Y_test = reorder[test_ex]
		clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, \
		verbose=0, max_iter=10000, tol=1e-4)
		clf.fit(X, Y)
		#print(clf.coef_.shape)
		#dec = clf.decision_function(mat2)
		#print(nt*num_class, nt*num_class+num_class)
		#dec_1[:, nt*num_class:nt*num_class+num_class] = dec
		coef_1[nt*num_class:nt*num_class+num_class,:] = clf.coef_


	coef_avg = np.zeros((num_class, mat2.shape[1]), dtype="float32")
	for nt in range(num_times):
		for i in range(num_class):
			coef_avg[i,:] = coef_avg[i,:]+coef_1[nt*num_class+i,:]
	coef_avg = coef_avg / num_times

	#g = sns.clustermap(coef_avg, cmap="plasma")
	#plt.show()

	px_int = pd.read_csv("/home/qzhu/Downloads/morphology.dec6/mat3_2.leiden.2.5.csv", index_col=0, dtype="int32").to_numpy().flatten()
	t_max = np.max(px_int)
	c_max = np.max(reorder)
	#X is 4096 by number of cells
	X = read_from_file_mode(mode="nissl", brain=3)
	#coef_avg_dapi = coef_avg[:, :4096]
	coef_avg_nissl = coef_avg[:, 4096:]
	#coef_avg_nissl = coef_avg
	weighted_X = np.copy(X)
	print(X.shape)
	print(coef_avg_nissl.shape)

	agg_mat = np.empty((coef_avg_nissl.shape[0], t_max+1), dtype="float32")
	print(agg_mat.shape)
	for i in range(t_max+1):
		t_ids = np.where(px_int==i)[0]
		agg_mat[:,i] = np.mean(coef_avg_nissl[:, t_ids], axis=1)
	g = sns.clustermap(agg_mat, cmap="plasma")
	plt.show()
	
	'''
	agg_mat = np.empty((weighted_X.shape[1], t_max+1), dtype="float32")
	for i in range(t_max+1):
		t_ids = np.where(px_int==i)[0]
		agg_mat[:,i] = np.mean(weighted_X[t_ids, :], axis=0)
	c_mat = np.empty((c_max, agg_mat.shape[1]), dtype="float32")
	for i in range(1,c_max+1):
		t_ids = np.where(reorder==i)[0]
		c_mat[i-1,:] = np.mean(agg_mat[t_ids, :], axis=0)
	g = sns.clustermap(c_mat, cmap="plasma")
	plt.show()
	'''
	for i in range(1, c_max+1):
		t_ids = np.where(reorder==i)[0]
		for j in range(4096):
			weighted_X[j, t_ids] = weighted_X[j, t_ids] * coef_avg_nissl[i-1, j]

	agg_mat = np.empty((weighted_X.shape[1], t_max+1), dtype="float32")
	for i in range(t_max+1):
		t_ids = np.where(px_int==i)[0]
		agg_mat[:,i] = np.mean(weighted_X[t_ids, :], axis=0)
	#for i in range(agg_mat.shape[1]):
	#	if np.sum(agg_mat[:,i])==0: continue
	#	agg_mat[:,i] = agg_mat[:,i] / np.max(agg_mat[:,i])
	c_mat = np.empty((c_max, agg_mat.shape[1]), dtype="float32")
	for i in range(1,c_max+1):
		t_ids = np.where(reorder==i)[0]
		c_mat[i-1,:] = np.mean(agg_mat[t_ids, :], axis=0)

	g = sns.clustermap(c_mat, cmap="plasma")
	plt.show()


	print("Done")

	reorder = domain_reorder
	num_class = np.unique(reorder).shape[0]
	dec_2 = np.empty((reorder.shape[0], num_class * num_times), dtype="float32")
	print("dec_2 shape", dec_2.shape)
	for nt in range(num_times):
		print("Time", nt)
		c_size = []
		train_ex = []
		test_ex = []
		for i in range(num_class):
			n1 = np.where(reorder==i+1)[0]
			num_train = int(n1.shape[0] * ratio)
			c_size.append(num_train)
			this_train_ex = np.random.choice(n1, num_train, replace=False)
			this_test_ex = np.setdiff1d(n1, this_train_ex)
			train_ex.extend(this_train_ex)
			test_ex.extend(this_test_ex)
		train_ex = np.array(train_ex)
		test_ex = np.array(test_ex)
		X = mat2[train_ex, :]
		X_test = mat2[test_ex, :]
		Y = reorder[train_ex]
		Y_test = reorder[test_ex]
		clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, \
		verbose=0, max_iter=10000, tol=1e-4)
		clf.fit(X, Y)
		dec = clf.decision_function(mat2)
		print(nt*num_times, nt*num_times+num_class)
		#dec_2[:, nt*num_times:nt*num_times+num_class] = dec
		dec_2[:, nt*num_class:nt*num_class+num_class] = dec

		

	'''
	print(dec.shape)

	
	X = mat2
	Y = domain_reorder
	clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, \
	verbose=0, max_iter=10000, tol=1e-4)
	clf.fit(X, Y)
	pr2 = clf.predict(X)
	dec2 = clf.decision_function(X)

	print(dec2.shape)
	'''
	new_dec = np.empty((dec_1.shape[0], dec_1.shape[1]+dec_2.shape[1]), dtype="float32")
	new_dec[:, 0:dec_1.shape[1]] = dec_1
	new_dec[:, dec_1.shape[1]:] = dec_2

	X_emb = TSNE(n_components=2, perplexity=50).fit_transform(new_dec)
	#X_emb = umap.UMAP(n_neighbors=20, min_dist=0.5).fit_transform(new_dec)
	plt.scatter(X_emb[:,0], X_emb[:,1], s=20, cmap="tab20b", c=ct_reorder, alpha=1.0)
	plt.show()
	plt.scatter(X_emb[:,0], X_emb[:,1], s=20, cmap="tab20b", c=domain_reorder, alpha=1.0)
	plt.show()

	sys.exit(0)

	#model = Ivis(n_epochs_without_progress=20, ntrees=100, k=15, supervision_weight=0.9)
	#model = Ivis(n_epochs_without_progress=20, supervision_metric="categorical_hinge", supervision_weight=0.8)
	#model.fit(mat2, clust_id)
	#X_emb = model.transform(mat2)
	plt.scatter(X_emb[:,0], X_emb[:,1], s=20, cmap="tab20c", c=original, alpha=1.0)
	plt.show()

	#print(em.shape)
	sys.exit(0)


	nrow = 4
	ncol = 4
	size_factor=10
	f, axn = plt.subplots(nrow, ncol, figsize=(ncol * size_factor, nrow * size_factor))
	plt.subplots_adjust(hspace=0.01, wspace=0.01)
	#cm = plt.cm.get_cmap(colormap)
	ct = 0
	for i in np.unique(clust_id):
		X = np.zeros(clust_id.shape, dtype="int32")
		m = np.where(clust_id==i)[0]
		m2 = np.where(clust_id!=i)[0]
		X[m] = 1
		X[m2] = 0

		X_r = X.copy()
		Xcen = X_emb.copy()
		ind = 0
		
		for j in range(m2.shape[0]):
			X_r[ind] = 0
			#Xcen[ind,:] = [X_emb[m2[j],0], X_emb[m2[j],1]]
			Xcen[ind,:] = [Xcell_reorder[m2[j],0], Xcell_reorder[m2[j],1]]
			ind+=1
		
		for j in range(m.shape[0]):
			X_r[ind] = 1
			#Xcen[ind,:] = [X_emb[m[j],0], X_emb[m[j],1]]
			Xcen[ind,:] = [Xcell_reorder[m[j],0], Xcell_reorder[m[j],1]]
			ind+=1

		max_id = np.max(clust_id) + 1
		axn.flat[ct].scatter(Xcen[:,0], Xcen[:,1], s=10, c=X_r, cmap="tab20", vmin=0, vmax=max_id)
		axn.flat[ct].annotate("cluster %d" % ct, (0.5, 0.95), ha="center")
		axn.flat[ct].title.set_visible(False)
		axn.flat[ct].set_facecolor("white")
		ct+=1
		print(i)

	plt.show()
	sys.exit(0)

	expr_mat = pd.read_csv("../cortex_expression_zscore.csv", sep=",", header=0, index_col=0)

	genes = [g for g in expr_mat.index]

	#print genes	
	expr = {}
	for g in genes:
		expr[g] = expr_mat.loc[g].values
		#np.random.shuffle(expr[g])
		#print expr[g]
		#expr[g] = reader.read_expression("/home/qzhu/Downloads/brain_single/hmrfem.whole.brain.e.fd0-48.correlated.gene/pca_corrected_z_score_cortex/fcortex.gene.%s.txt" % g)
		#expr[g] = expr[g][1:]

		'''
		for i in range(expr[g].shape[0]):
			if expr[g][i]<0:
				expr[g][i] = 0
		'''
		#print expr[g]
	print("Finished reading gene expression.")
	#sys.exit(0)

	'''
	ct_reorder = np.array(ct_reorder)
	ct2_reorder = np.array(ct2_reorder)
	domain_reorder = np.array(domain_reorder)
	'''

	ratio = 0.8 #for training
	show_cell_type = False
	num_class = 9
	domain_reorder = np.array(domain_reorder)
	reorder = domain_reorder
	if show_cell_type:
		#num_class = 8
		num_class = np.unique(ct).shape[0]
		reorder = ct_reorder

	num_times = 1

	#ct_label_map = {'Microglia': 5, 'Combined Oligodendrocyte': 3, \
	#'Endothelial Cell': 4, 'Astrocyte': 1, 'Combined Neuron': 2}

	#genes2 = [g for g in genes]
	#genes2 = reader.read_genes("2.expr.3807genes.pca.output.genes.list") #all the genes
	genes2 = reader.read_genes("genes.top.1000")
	#random.shuffle(genes2)
	num_expr_class = 3
	for ig, g in enumerate(genes2):
		auc_perf, pr_perf = [], []
		n_auc_perf, n_pr_perf = [], []

		early_exit = False

		print("Gene", ig)
		for nt in range(num_times):
			#print "Time", nt
			c_size = []
			train_ex = []
			test_ex = []
			for i in range(num_class):
				n1 = np.where(reorder==i+1)[0]
				num_train = int(n1.shape[0] * ratio)
				c_size.append(num_train)
				this_train_ex = np.random.choice(n1, num_train, replace=False)
				this_test_ex = np.setdiff1d(n1, this_train_ex)
				train_ex.extend(this_train_ex)
				test_ex.extend(this_test_ex)
			train_ex = np.array(train_ex)
			test_ex = np.array(test_ex)

			X = mat2[train_ex, :]
			X_test = mat2[test_ex, :]
			Y = expr[g][train_ex]
			Y_test = expr[g][test_ex]

			tmp_Y = np.zeros(Y.shape, dtype="int32")
			tmp_Y_test = np.zeros(Y_test.shape, dtype="int32")
			for i in range(tmp_Y.shape[0]):
				if Y[i]>0.5:
					tmp_Y[i] = 3
				elif Y[i]<-0.5:
					tmp_Y[i] = 1
				else:
					tmp_Y[i] = 2
			for i in range(tmp_Y_test.shape[0]):
				if Y_test[i]>0.5:
					tmp_Y_test[i] = 3
				elif Y_test[i]<-0.5:
					tmp_Y_test[i] = 1
				else:
					tmp_Y_test[i] = 2

			Y = tmp_Y
			Y_test = tmp_Y_test
			#Y = reorder[train_ex]
			#Y_test = reorder[test_ex]

			#clf = SVR(kernel="poly", C=1e-5)
			#clf = Lasso(alpha=0.1)
			#clf = Ridge(alpha=1.0)

			'''
			clf = ElasticNet(alpha=0.1, l1_ratio=0.5)
			y_lin = clf.fit(X, Y).predict(X_test)
			r2 = r2_score(Y_test, y_lin)
			#r2 = clf.fit(X,Y).score(X_test, Y_test)	
			'''
			#print g, nt, r2
			clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, \
			verbose=0, max_iter=10000, tol=1e-4)
			clf.fit(X, Y)
			pr = clf.predict(X_test)
			dec = clf.decision_function(X_test)

			try:
				r_label = load_seqfishplus_new.get_label_order(pr, dec)
				auc_list, pr_list = load_seqfishplus_new.get_performance_eval(dec, Y_test, r_label)
				auc_perf.append(auc_list)
				pr_perf.append(pr_list)
			except ZeroDivisionError:
				early_exit = True

		if early_exit:
			continue				

		num_ex = len(auc_perf[0][0])
		avg_auc_perf = np.zeros((num_expr_class, num_ex, 2), dtype="float32")
		avg_pr_perf = np.zeros((num_expr_class, num_ex, 2), dtype="float32")
		for ne in range(num_ex):
			for c in range(num_expr_class):
				for ni in range(num_times):
					avg_auc_perf[c, ne, 0] += float(auc_perf[ni][c][ne][0]) / num_times
					avg_auc_perf[c, ne, 1] += float(auc_perf[ni][c][ne][1]) / num_times
					avg_pr_perf[c, ne, 0] += float(pr_perf[ni][c][ne][0]) / num_times
					avg_pr_perf[c, ne, 1] += float(pr_perf[ni][c][ne][1]) / num_times

		for c in range(num_expr_class):
			auc_y = avg_auc_perf[c, :, 0]	
			auc_x = avg_auc_perf[c, :, 1]
			print("Class", g, c+1, auc(auc_x, auc_y))

		#f,axn = plt.subplots(1,num_class)
		for c in range(num_expr_class):
			pr_y = avg_pr_perf[c, :, 0]	
			pr_x = avg_pr_perf[c, :, 1]
			print("Class", g, c+1, "PR", pr_y[5], pr_y[10], pr_y[15], pr_y[20])

		'''
		Y_2, Y_2_test = None, None
		if show_cell_type:
			Y_2 = ct2_reorder[train_ex]
			Y_2_test = ct2_reorder[test_ex]
			
		#print len(train_ex), len(test_ex)
		clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, \
		verbose=0, max_iter=10000, tol=1e-4)
		clf.fit(X, Y)
		pr = clf.predict(X_test)
		dec = clf.decision_function(X_test)

		r_label = get_label_order(pr, dec)
		auc_list, pr_list = get_performance_eval(dec, Y_test, r_label)
		auc_perf.append(auc_list)
		pr_perf.append(pr_list)

		if show_cell_type:
			is_neuron = np.where(Y==ct_label_map["Combined Neuron"])[0]
			clf_neuron = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, \
			verbose=0, max_iter=10000, tol=1e-4)
			X_neuron = X[is_neuron]
			Y_neuron = Y_2[is_neuron]
			clf_neuron.fit(X_neuron, Y_neuron)

			is_neuron_test = np.where(pr==ct_label_map["Combined Neuron"])[0]
			X_test_neuron = X_test[is_neuron_test]
			Y_test_neuron = Y_2_test[is_neuron_test]
			pr_neuron = clf_neuron.predict(X_test_neuron)
			dec_neuron = clf_neuron.decision_function(X_test_neuron)
			#print dec_neuron
			print dec_neuron.shape
			if dec_neuron.ndim==1:
				new_dec_neuron = np.empty((dec_neuron.shape[0], 2), dtype="float32")
				new_dec_neuron[:,0] = dec_neuron
				new_dec_neuron[:,1] = dec_neuron * -1.0
				dec_neuron = new_dec_neuron

			n_r_label = get_label_order(pr_neuron, dec_neuron)
			n_auc_list, n_pr_list = get_performance_eval(dec_neuron, Y_test_neuron, n_r_label)
			n_auc_perf.append(n_auc_list)
			n_pr_perf.append(n_pr_list)

		#all_neuron = np.where(pr==ct_label_map["Combined Neuron"])[0]
		#print all_neuron
		#print dec
		'''

	'''
	num_ex = len(auc_perf[0][0])
	avg_auc_perf = np.zeros((num_class, num_ex, 2), dtype="float32")
	avg_pr_perf = np.zeros((num_class, num_ex, 2), dtype="float32")
	for ne in range(num_ex):
		for c in range(num_class):
			for ni in range(num_times):
				avg_auc_perf[c, ne, 0] += float(auc_perf[ni][c][ne][0]) / num_times
				avg_auc_perf[c, ne, 1] += float(auc_perf[ni][c][ne][1]) / num_times
				avg_pr_perf[c, ne, 0] += float(pr_perf[ni][c][ne][0]) / num_times
				avg_pr_perf[c, ne, 1] += float(pr_perf[ni][c][ne][1]) / num_times

	if show_cell_type:
		num_ex = len(n_auc_perf[0][0])
		neuron_auc_perf = np.zeros((2, num_ex, 2), dtype="float32")
		neuron_pr_perf = np.zeros((2, num_ex, 2), dtype="float32")
		for ne in range(num_ex):
			for ni in range(num_times):
				for c in range(2):
					neuron_auc_perf[c, ne, 0] += float(n_auc_perf[ni][c][ne][0]) / num_times
					neuron_auc_perf[c, ne, 1] += float(n_auc_perf[ni][c][ne][1]) / num_times
					neuron_pr_perf[c, ne, 0] += float(n_pr_perf[ni][c][ne][0]) / num_times
					neuron_pr_perf[c, ne, 1] += float(n_pr_perf[ni][c][ne][1]) / num_times
				
	f,axn = plt.subplots(1,num_class)
	for c in range(num_class):
		auc_y = avg_auc_perf[c, :, 0]	
		auc_x = avg_auc_perf[c, :, 1]
		axn.flat[c].scatter(auc_x, auc_y)
		print "Class", c, auc(auc_x, auc_y)
	plt.show()

	f,axn = plt.subplots(1,num_class)
	for c in range(num_class):
		pr_y = avg_pr_perf[c, :, 0]	
		pr_x = avg_pr_perf[c, :, 1]
		axn.flat[c].scatter(pr_x, pr_y)
		print "Class", c, "PR", pr_y[5], pr_y[10], pr_y[15], pr_y[20]
	plt.show()

	if show_cell_type:
		f,axn = plt.subplots(1,2)
		for c in range(2):
			auc_y = neuron_auc_perf[c, :, 0]	
			auc_x = neuron_auc_perf[c, :, 1]
			axn.flat[c].scatter(auc_x, auc_y)
			print "Class", c, auc(auc_x, auc_y)
		plt.show()

		f,axn = plt.subplots(1,2)
		for c in range(2):
			pr_y = neuron_pr_perf[c, :, 0]	
			pr_x = neuron_pr_perf[c, :, 1]
			axn.flat[c].scatter(pr_x, pr_y)
			print "Class", c, "PR", pr_y[5], pr_y[10], pr_y[15], pr_y[20]
		plt.show()
	'''
	#====================================================

	'''
	num_cor = 0
	cor = {}
	tot = {}
	for ip,truth in zip(pr, Y_test):
		#print ip, truth
		cor.setdefault(truth, 0)
		tot.setdefault(truth, 0)
		tot[truth]+=1
		if ip==truth:
			num_cor+=1
			cor[truth]+=1	
	print num_cor, "out of", len(pr)
	for c in cor:
		print c, cor[c], "out of", tot[c]
	'''

	'''
	pca = PCA(n_components=100)
	pca.fit(mat2)
	mat_new = pca.transform(mat2)
	#mat_new = mat_new[:,1:100]
	print pca.components_.shape
	print pca.explained_variance_ratio_
	print mat_new.shape

	tsne = TSNE(n_components=2)
	mat_new = tsne.fit_transform(mat_new)
	#mat_new = tsne.fit_transform(mat2)

	domain_reorder = np.array(domain_reorder)
	f,axn = plt.subplots(3,3)
	titles = ["Astro", "Endo", "GABA-N", "Glut-N", "Micro", "Oligo.1", "Oligo.2", "Oligo.3"]
	show_cell_type = True

	reorder = ct_reorder
	if not show_cell_type:
		reorder = domain_reorder
		titles = ["O2", "I1a", "O4", "I1b", "O1", "I2", "I3", "O3", "IS"]

	for i in range(np.unique(reorder).shape[0]):
		#n1 = np.where(domain_reorder==i+1)[0]
		#n1_not = np.where(domain_reorder!=i+1)[0]
		#dr = np.copy(domain_reorder)
		#n1 = np.where(ct_reorder==i+1)[0]
		#n1_not = np.where(ct_reorder!=i+1)[0]
		#dr = np.copy(ct_reorder)
		n1 = np.where(reorder==i+1)[0]
		n1_not = np.where(reorder!=i+1)[0]
		dr = np.copy(reorder)

		print i+1, n1.shape[0]
		dr[n1] = 1
		dr[n1_not] = 0
		xval = mat_new[:,0]
		yval = mat_new[:,1]
		t_mat_1 = mat_new[n1,:]
		t_mat_0 = mat_new[n1_not,:]
		pc1,pc2 = 0,1
		#axn.flat[i].scatter(xval, yval, c=dr, s=5)
		#axn.flat[i].text(np.max(xval), np.max(yval), "D%d" % (i+1))
		if t_mat_1.shape[0]<=2:
			axn.flat[i].scatter(t_mat_0[:,pc1], t_mat_0[:,pc2], c="black", s=5)
			axn.flat[i].scatter(t_mat_1[:,pc1], t_mat_1[:,pc2], c="red", s=5)
		else:
			axn.flat[i] = sns.kdeplot(t_mat_0[:,pc1], t_mat_0[:,pc2], shade=True, ax=axn.flat[i], cmap="Blues", alpha=0.5, shade_lowest=False)
			axn.flat[i] = sns.kdeplot(t_mat_1[:,pc1], t_mat_1[:,pc2], shade=True, ax=axn.flat[i], cmap="Reds", alpha=0.5, shade_lowest=False)
		axn.flat[i].text(np.max(t_mat_0[:,pc1]), np.max(t_mat_0[:,pc2]), titles[i])
	plt.show()
	'''
