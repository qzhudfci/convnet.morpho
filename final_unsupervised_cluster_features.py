#!/usr/bin/python
import sys
import numpy as np
sys.setrecursionlimit(10000)
import scanpy as sc
import load_brain2_new
import load_brain1_new
import load_seqfishplus_new

def shuffle(df, n=1, axis=0):
	df = df.copy()
	for _ in range(n):
		df.apply(np.random.shuffle, axis=axis)
	return df

def do_and_plot_leiden(mat, file_prefix="mat1"):
	adata = sc.AnnData(X=mat)
	sc.pp.neighbors(adata, use_rep='X', metric="cosine", n_neighbors=50, random_state=None)
	print("Done pp neighbors")
	sc.tl.umap(adata, min_dist=0.1, random_state=None)
	print("Done umap")
	sc.tl.leiden(adata, resolution=1.0, key_added = "leiden_1.0")
	print("Done leiden 1.0")
	sc.tl.leiden(adata, resolution=1.4, key_added = "leiden_1.4")
	print("Done leiden 1.4")
	sc.tl.leiden(adata, resolution=2.0, key_added = "leiden_2.0")
	print("Done leiden 2.0")
	sc.tl.leiden(adata, resolution=2.5, key_added = "leiden_2.5")
	print("Done leiden 2.5")
	#sc.pl.umap(adata, color=['leiden_1.0','leiden_1.4', 'leiden_2.0', "leiden_2.5"])
	sc.pl.umap(adata, color=['leiden_2.0', "leiden_2.5"])

	adata.obs["leiden_1.0"].to_csv("test.%s.leiden.1.0.csv" % file_prefix)
	adata.obs["leiden_1.4"].to_csv("test.%s.leiden.1.4.csv" % file_prefix)
	adata.obs["leiden_2.0"].to_csv("test.%s.leiden.2.0.csv" % file_prefix)
	adata.obs["leiden_2.5"].to_csv("test.%s.leiden.2.5.csv" % file_prefix)

def read_all_png_list():
	id_to_png_1 = read_png_list(brain="1")
	id_to_png_2 = read_png_list(brain="2")
	id_to_png_3 = read_png_list(brain="3")
	all_id_to_png = id_to_png_1 + id_to_png_2 + id_to_png_3
	return all_id_to_png	

def read_png_list(brain="1"):
	file_png_list = ""
	if brain=="1":
		file_png_list = "brain1.segmented/png.file.list.2"
	elif brain=="2":
		file_png_list = "brain2.segmented/png.file.list.2"
	elif brain=="3":
		file_png_list = "seqfishplus/png.file.list.2"
	f = open(file_png_list)
	id_to_png = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		id_to_png.append((ll[0], ll[1]))
	f.close()
	return id_to_png

def read_pair(n1, n2): #_1 which is dapi, _2 which is nissl
	f = open(n1)
	f.readline()
	t_map_1 = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split(",")
		t_map_1.setdefault(int(ll[1]), [])
		t_map_1[int(ll[1])].append(int(ll[0]))
	f.close()
	f = open(n2)
	f.readline()
	t_map_2 = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split(",")
		t_map_2.setdefault(int(ll[1]), [])
		t_map_2[int(ll[1])].append(int(ll[0]))
	f.close()
	return t_map_1, t_map_2
	

if __name__=="__main__":
	b1_mat2, b1_ct_reorder, b1_ct2_reorder, b1_domain_reorder, b1_Xcell_reorder, b1_num_cell = load_brain1_new.load(filter_feature=False, log=False, option=1)
	b2_mat2, b2_ct_reorder, b2_ct2_reorder, b2_domain_reorder, b2_Xcell_reorder, b2_num_cell = load_brain2_new.load(filter_feature=False, log=False, option=1)
	b3_mat2, b3_ct_reorder, b3_domain_reorder, b3_Xcell_reorder, b3_num_cell = load_seqfishplus_new.load(filter_feature=False, log=False, use_equalize=True)

	mat3 = np.empty((b1_num_cell+b2_num_cell+b3_num_cell, 8192), dtype="float32")
	mat3[0:b1_num_cell, :] = b1_mat2
	mat3[b1_num_cell:b1_num_cell+b2_num_cell, :] = b2_mat2
	mat3[b1_num_cell+b2_num_cell:, :] = b3_mat2

	mat3_1 = np.transpose(mat3[:, 0:4096])
	mat3_2 = np.transpose(mat3[:, 4096:])

	print("Doing 0:4096")
	do_and_plot_leiden(mat3_1, file_prefix="mat3_1")
	print("Doing 4096:8192")
	do_and_plot_leiden(mat3_2, file_prefix="mat3_2")

	
