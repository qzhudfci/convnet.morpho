import sys
import os
import numpy as np
import scipy
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import random
import sys
import scipy
import scipy.stats
from scipy.interpolate import interp1d
from sklearn.decomposition import PCA
from sklearn.utils.extmath import randomized_svd
from sklearn.manifold import TSNE
from sklearn.metrics import auc
from scipy.stats import pearsonr
import os
sys.setrecursionlimit(10000)
from sklearn import svm
from sklearn.svm import SVR
from sklearn.metrics import r2_score
from sklearn.calibration import CalibratedClassifierCV
from sklearn.linear_model import Lasso, Ridge, ElasticNet
from sklearn.datasets import make_regression
from sklearn.ensemble import GradientBoostingRegressor, GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from operator import itemgetter
import load_brain1_new
import reader

def shuffle(df, n=1, axis=0):
	df = df.copy()
	for _ in range(n):
		df.apply(np.random.shuffle, axis=axis)
	return df

def get_performance_eval(dec, true_label, labels):
	num_class = labels.shape[0]
	num_example = dec.shape[0]
	auc_eval = {}
	pr_eval = {}
	fold_pr_eval = {}
	for c in range(num_class):
		if num_class>2:
			st = list(zip(true_label, dec[:,c]))
		else: #num_class==2
			if c==0:
				st = list(zip(true_label, dec*-1))
			else:
				st = list(zip(true_label, dec))
		st.sort(key=itemgetter(1), reverse=True)
		tp, fp = 0, 0
		cond_pos = 0
		cond_neg = 0
		this_auc = []
		this_pr = []
		for ex in range(num_example):
			if st[ex][0]==labels[c]: #st[ex][0] is true_label
				cond_pos+=1
			else:
				cond_neg+=1
		bg = float(cond_pos) / num_example 
		for ex in range(num_example):
			if st[ex][0]==labels[c]:
				tp+=1
			else:
				fp+=1
			recall = float(tp) / cond_pos
			precision = float(tp) / (ex + 1)
			tpr = recall
			fpr = float(fp) / cond_neg
			this_pr.append((precision, recall))
			this_auc.append((tpr, fpr))
		rev_ex = num_example - 2
		while rev_ex>=0:
			t_t = this_pr[rev_ex]
			t_t_plus_1 = this_pr[rev_ex + 1]
			if t_t[0]<t_t_plus_1[0]:
				t_t = (t_t_plus_1[0], t_t[1])
				this_pr[rev_ex] = t_t
			rev_ex = rev_ex - 1

		convert_pr = {}
		for ij,ik in this_pr:
			if ik==0:
				continue
			convert_pr.setdefault(ik, [])
			convert_pr[ik].append(ij)
		#good_pr = []
		#for sort_k in sorted(convert_pr.keys()):
		#	good_pr.append((max(convert_pr[sort_k]), sort_k))
		pr_y = [max(convert_pr[sort_k]) for sort_k in sorted(convert_pr.keys())]
		pr_x = [sort_k for sort_k in sorted(convert_pr.keys())]
		foldpr_y = [float(t1 / bg) for t1 in pr_y]

		interp_x = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
		interp_y = np.interp(interp_x, pr_x, pr_y)	
		interp_fold_y = np.interp(interp_x, pr_x, foldpr_y)

		#print(interp_y)
		auc_eval[labels[c]] = this_auc
		pr_eval[labels[c]] = list(zip(interp_y, interp_x))
		fold_pr_eval[labels[c]] = list(zip(interp_fold_y, interp_x))
	return auc_eval, pr_eval, fold_pr_eval

def get_label_order(pr, dec): #labels must start from 1, and increase up to num_class
	num_class = dec.shape[1]
	num_example = dec.shape[0]
	r_label = list(range(1, num_class+1))
	return r_label

def get_reverse_map(ct):
	rev = {}
	for i in ct.keys():
		rev[ct[i]] = i
	return rev

def get_fold_pr(pr, Y):
	tot = {}
	for i in range(Y.shape[0]):
		tot.setdefault(Y[i], 0)
		tot[Y[i]]+=1
	fold = []
	for i in range(pr.shape[0]):
		bg = 1/tot[Y[i]]
		fold.append(pr[i] / bg)
	return fold

def fix_title(title):
	if "Combined" in title:
		title = title.split()[1]
	elif title=="Endothelial Cell":
		title = "Endothelial"
	return title


if __name__=="__main__":
	font = {"family": "Liberation Sans"}
	matplotlib.rc("font", **font)

	mat2, ct_reorder, ct2_reorder, domain_reorder, Xcell_reorder, num_cell = load_brain1_new.load(filter_feature=False, log=True, zscore1=False)
	ct_map = {'Astrocyte': 1, 'Combined Neuron': 2, 'Combined Oligodendrocyte': 3, 'Endothelial Cell': 4, 'Microglia': 5}
	ct2_map = {'Astrocyte': 1, 'Endothelial Cell': 2, 'GABA-ergic Neuron': 3, 'Glutamatergic Neuron': 4, 'Microglia': 5, 'Oligodendrocyte.1': 6, 'Oligodendrocyte.2': 7, 'Oligodendrocyte.3': 8}

	domain_map = {"L5":1, "L4":2, "L2/3":3, "L1/L6b upper":4, "L1/L6b lower":5, "L6b":6, "L6a":7, "L5,L6a":8, "L4.2":9}
	

	ct_rev_map = get_reverse_map(ct_map)
	ct2_rev_map = get_reverse_map(ct2_map)
	domain_rev_map = get_reverse_map(domain_map)
	#expr_mat = pd.read_csv("cortex_expression_zscore.csv", sep=",", header=0, index_col=0)
	#genes = [g for g in expr_mat.index]

	#expr = {}
	#for g in genes:
	#	expr[g] = expr_mat.loc[g].values
	#print("Finished reading gene expression.")
	'''
	ct_reorder = np.array(ct_reorder)
	ct2_reorder = np.array(ct2_reorder)
	domain_reorder = np.array(domain_reorder)
	'''
	ratio = 0.8 #for training
	show_cell_type = False
	domain_reorder = np.array(domain_reorder)
	reorder = domain_reorder
	if show_cell_type:
		reorder = ct_reorder
		#reorder = ct2_reorder
	num_times = 10

	#ct_label_map = {'Microglia': 5, 'Combined Oligodendrocyte': 3, \
	#'Endothelial Cell': 4, 'Astrocyte': 1, 'Combined Neuron': 2}
	#genes2 = [g for g in genes]
	#genes2 = reader.read_genes("2.expr.3807genes.pca.output.genes.list")
	#random.shuffle(genes2)

	auc_perf, pr_perf, fold_pr_perf = [], [], []
	
	for nt in range(num_times):
		#print "Time", nt
		train_ex = []
		test_ex = []
		for c in np.unique(reorder):
			n1 = np.where(reorder==c)[0]
			num_train = int(n1.shape[0] * ratio)
			if num_train==0:
				continue
			this_train_ex = np.random.choice(n1, num_train, replace=False)
			this_test_ex = np.setdiff1d(n1, this_train_ex)
			train_ex.extend(this_train_ex)
			test_ex.extend(this_test_ex)
		train_ex = np.array(train_ex)
		test_ex = np.array(test_ex)

		X = mat2[train_ex, :]
		X_test = mat2[test_ex, :]

		Y = reorder[train_ex]
		Y_test = reorder[test_ex]

		clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, \
		verbose=0, max_iter=10000, tol=1e-4)
		clf.fit(X, Y)
		pr = clf.predict(X_test)
		dec = clf.decision_function(X_test)
			
		r_label = clf.classes_
		auc_list, pr_list, fold_pr_list = get_performance_eval(dec, Y_test, r_label)
		auc_perf.append(auc_list)
		pr_perf.append(pr_list)
		fold_pr_perf.append(fold_pr_list)
		print(nt)
		#print("Score average", np.mean(cor))

		
	key1 = list(auc_perf[0].keys())[0]
	num_auc_ex = len(auc_perf[0][key1])

	avg_auc_perf, avg_pr_perf, avg_fold_pr_perf = {}, {}, {}

	for c in auc_perf[0].keys():
		avg_auc_perf[c] = np.zeros((num_auc_ex, 2), dtype="float32")
	for c in pr_perf[0].keys():
		num_pr_ex = max([len(pr_perf[ni][c]) for ni in range(num_times)])
		avg_pr_perf[c] = np.zeros((num_pr_ex, 2), dtype="float32")

		num_fold_pr_ex = max([len(fold_pr_perf[ni][c]) for ni in range(num_times)])
		avg_fold_pr_perf[c] = np.zeros((num_fold_pr_ex, 2), dtype="float32")


	for ni in range(num_times):
		for c in auc_perf[ni].keys():
			for ne in range(num_auc_ex):
				avg_auc_perf[c][ne, 0] += float(auc_perf[ni][c][ne][0]) / num_times
				avg_auc_perf[c][ne, 1] += float(auc_perf[ni][c][ne][1]) / num_times

	for ni in range(num_times):
		for c in pr_perf[ni].keys():
			for ne in range(len(pr_perf[ni][c])):
				avg_pr_perf[c][ne, 0] += float(pr_perf[ni][c][ne][0]) / num_times
				avg_pr_perf[c][ne, 1] += float(pr_perf[ni][c][ne][1]) / num_times

	for ni in range(num_times):
		for c in fold_pr_perf[ni].keys():
			for ne in range(len(fold_pr_perf[ni][c])):
				avg_fold_pr_perf[c][ne, 0] += float(fold_pr_perf[ni][c][ne][0]) / num_times
				avg_fold_pr_perf[c][ne, 1] += float(fold_pr_perf[ni][c][ne][1]) / num_times

	'''
	ind_auc = []
	for ni in range(num_times):
		c=3 #class is equal to 3
		avg_auc_1 = np.zeros((num_ex, 2), dtype="float32")
		for ne in range(num_ex):
			avg_auc_1[ne,0] = float(auc_perf[ni][c][ne][0])
			avg_auc_1[ne,1] = float(auc_perf[ni][c][ne][1])
		auc_y = avg_auc_1[:,0]
		auc_x = avg_auc_1[:,1]
		ind_auc.append(auc(auc_x, auc_y))
	'''
	#print("Gene", ig, g)
	#print("Total", num_3.shape[0] + num_3_test.shape[0])
	#print(" ".join(["%.5f" % v for v in ind_auc]))

	num_class = len(avg_auc_perf.keys())

	#plot ROC curve======================
	

	nrow = int(num_class/5)
	if num_class%5>0:
		nrow+=1
	f,axn = plt.subplots(nrow, 5, sharex=True, sharey=True, figsize=(8,2.5*nrow))
	if nrow==1:
		plt.subplots_adjust(top=0.88,bottom=0.195,left=0.11,right=0.9)
	elif nrow==2:
		plt.subplots_adjust(top=0.88,bottom=0.195,left=0.11,right=0.9)
		

	bar_labels = []
	bar_counts = []
	for ic,c in enumerate(avg_auc_perf.keys()):
		auc_y = avg_auc_perf[c][:, 0]	
		auc_x = avg_auc_perf[c][:, 1]

		axn.flat[ic].scatter(auc_x, auc_y, color="red", s=4)
		axn.flat[ic].plot(auc_x, auc_y, "-", linewidth=1, color="red")
		title = ""
		if show_cell_type:
			title = fix_title(ct_rev_map[c])
		else:
			title = fix_title(domain_rev_map[c])
		print("Class", c, auc(auc_x, auc_y))
		axn.flat[ic].set_title(title)
		bar_counts.append(auc(auc_x, auc_y))
		bar_labels.append(title)

	fname = ""
	if show_cell_type==False:
		plt.setp(axn[-1,:], xlabel="1-Specificity")
		plt.setp(axn[:,0], ylabel="Sensitivity")
		fname = "domain"
	else:
		plt.setp(axn[0], xlabel="1-Specificity")
		plt.setp(axn[0], ylabel="Sensitivity")
		fname = "cell.type"

	plt.savefig("brain1.%s.auroc.png" % fname, dpi=300)
	plt.show()

	plt.figure(1,figsize=(6,1.5*nrow))
	if nrow==1:
		plt.subplots_adjust(top=0.88,bottom=0.295,left=0.195,right=0.9)
	else:
		plt.subplots_adjust(top=0.88,bottom=0.295,left=0.195,right=0.9)
		
	plt.barh(bar_labels, bar_counts, color="red")
	plt.xlabel("AUC")
	plt.savefig("bar.auc.brain1.%s.png" % fname, dpi=300)
	plt.show()

	#f,axn = plt.subplots(1,num_class)
	
	f,axn = plt.subplots(nrow, 5, sharex=True, sharey=False, figsize=(8,2.5*nrow))
	if nrow==1:
		plt.subplots_adjust(top=0.88,bottom=0.195,left=0.11,right=0.9)
	elif nrow==2:
		plt.subplots_adjust(top=0.88,bottom=0.195,left=0.11,right=0.9)
	bar_labels = []
	bar_counts = []
	for ic,c in enumerate(avg_pr_perf.keys()):
		fold_pr_y = avg_fold_pr_perf[c][:, 0]	
		fold_pr_x = avg_fold_pr_perf[c][:, 1]
		#axn.flat[ic].scatter(pr_x, pr_y)
		#print("Class", c, "PR", pr_y[5], pr_y[10], pr_y[15], pr_y[20])
		axn.flat[ic].scatter(fold_pr_x, fold_pr_y, color="blue", s=4)
		axn.flat[ic].plot(fold_pr_x, fold_pr_y, "-", linewidth=1, color="blue")
		title = ""
		if show_cell_type:
			title = fix_title(ct_rev_map[c])
		else:
			title = fix_title(domain_rev_map[c])
		print("Class", c, "PR", fold_pr_y[0])
		axn.flat[ic].set_title(title)
		bar_counts.append(fold_pr_y[0])
		bar_labels.append(title)

	fname = ""
	if show_cell_type==False:
		plt.setp(axn[-1,:], xlabel="Recall")
		plt.setp(axn[:,0], ylabel="Fold precision over random")
		fname = "domain"
	else:
		plt.setp(axn[0], xlabel="Recall")
		plt.setp(axn[0], ylabel="Fold precision over random")
		fname = "cell.type"
		
	plt.savefig("brain1.%s.prcurve.png" % fname, dpi=300)
	plt.show()

	plt.figure(1,figsize=(6,1.5*nrow))
	if nrow==1:
		plt.subplots_adjust(top=0.88,bottom=0.295,left=0.195,right=0.9)
	else:
		plt.subplots_adjust(top=0.88,bottom=0.295,left=0.195,right=0.9)
	plt.barh(bar_labels, bar_counts)
	plt.xlabel("Fold precision over random at 10% recall")
	plt.savefig("bar.pr.brain1.%s.png" % fname, dpi=300)
	plt.show()
	


	outfile = "domain"
	if show_cell_type:
		outfile = "cell.type"

	'''
	fw = open("output/%s.pr.txt" % outfile, "w")
	for ic,c in enumerate(avg_pr_perf.keys()):
		pr_y = avg_pr_perf[c][:, 0]	
		pr_x = avg_pr_perf[c][:, 1]
		fw.write("Class %s\n" % c)
		fw.write("x\t")
		fw.write("\t".join(["%.5f" % v for v in pr_x]) + "\n")
		fw.write("y\t")
		fw.write("\t".join(["%.5f" % v for v in pr_y]) + "\n")
	fw.close()

	fw = open("output/%s.auc.txt" % outfile, "w")
	for ic,c in enumerate(avg_auc_perf.keys()):
		auc_y = avg_auc_perf[c][:, 0]	
		auc_x = avg_auc_perf[c][:, 1]
		fw.write("Class %s\n" % c)
		fw.write("x\t")
		fw.write("\t".join(["%.5f" % v for v in auc_x]) + "\n")
		fw.write("y\t")
		fw.write("\t".join(["%.5f" % v for v in auc_y]) + "\n")
	fw.close()
	'''
	sys.stdout.flush()
	sys.exit(0)	
	

	'''
	num_ex = len(auc_perf[0][0])
	avg_auc_perf = np.zeros((num_class, num_ex, 2), dtype="float32")
	avg_pr_perf = np.zeros((num_class, num_ex, 2), dtype="float32")
	for ne in range(num_ex):
		for c in range(num_class):
			for ni in range(num_times):
				avg_auc_perf[c, ne, 0] += float(auc_perf[ni][c][ne][0]) / num_times
				avg_auc_perf[c, ne, 1] += float(auc_perf[ni][c][ne][1]) / num_times
				avg_pr_perf[c, ne, 0] += float(pr_perf[ni][c][ne][0]) / num_times
				avg_pr_perf[c, ne, 1] += float(pr_perf[ni][c][ne][1]) / num_times

	if show_cell_type:
		num_ex = len(n_auc_perf[0][0])
		neuron_auc_perf = np.zeros((2, num_ex, 2), dtype="float32")
		neuron_pr_perf = np.zeros((2, num_ex, 2), dtype="float32")
		for ne in range(num_ex):
			for ni in range(num_times):
				for c in range(2):
					neuron_auc_perf[c, ne, 0] += float(n_auc_perf[ni][c][ne][0]) / num_times
					neuron_auc_perf[c, ne, 1] += float(n_auc_perf[ni][c][ne][1]) / num_times
					neuron_pr_perf[c, ne, 0] += float(n_pr_perf[ni][c][ne][0]) / num_times
					neuron_pr_perf[c, ne, 1] += float(n_pr_perf[ni][c][ne][1]) / num_times
				
	f,axn = plt.subplots(1,num_class)
	for c in range(num_class):
		auc_y = avg_auc_perf[c, :, 0]	
		auc_x = avg_auc_perf[c, :, 1]
		axn.flat[c].scatter(auc_x, auc_y)
		print "Class", c, auc(auc_x, auc_y)
	plt.show()

	f,axn = plt.subplots(1,num_class)
	for c in range(num_class):
		pr_y = avg_pr_perf[c, :, 0]	
		pr_x = avg_pr_perf[c, :, 1]
		axn.flat[c].scatter(pr_x, pr_y)
		print "Class", c, "PR", pr_y[5], pr_y[10], pr_y[15], pr_y[20]
	plt.show()

	if show_cell_type:
		f,axn = plt.subplots(1,2)
		for c in range(2):
			auc_y = neuron_auc_perf[c, :, 0]	
			auc_x = neuron_auc_perf[c, :, 1]
			axn.flat[c].scatter(auc_x, auc_y)
			print "Class", c, auc(auc_x, auc_y)
		plt.show()

		f,axn = plt.subplots(1,2)
		for c in range(2):
			pr_y = neuron_pr_perf[c, :, 0]	
			pr_x = neuron_pr_perf[c, :, 1]
			axn.flat[c].scatter(pr_x, pr_y)
			print "Class", c, "PR", pr_y[5], pr_y[10], pr_y[15], pr_y[20]
		plt.show()
	'''
	#====================================================

	'''
	num_cor = 0
	cor = {}
	tot = {}
	for ip,truth in zip(pr, Y_test):
		#print ip, truth
		cor.setdefault(truth, 0)
		tot.setdefault(truth, 0)
		tot[truth]+=1
		if ip==truth:
			num_cor+=1
			cor[truth]+=1	
	print num_cor, "out of", len(pr)
	for c in cor:
		print c, cor[c], "out of", tot[c]
	'''

	'''
	pca = PCA(n_components=100)
	pca.fit(mat2)
	mat_new = pca.transform(mat2)
	#mat_new = mat_new[:,1:100]
	print pca.components_.shape
	print pca.explained_variance_ratio_
	print mat_new.shape

	tsne = TSNE(n_components=2)
	mat_new = tsne.fit_transform(mat_new)
	#mat_new = tsne.fit_transform(mat2)

	domain_reorder = np.array(domain_reorder)
	f,axn = plt.subplots(3,3)
	titles = ["Astro", "Endo", "GABA-N", "Glut-N", "Micro", "Oligo.1", "Oligo.2", "Oligo.3"]
	show_cell_type = True

	reorder = ct_reorder
	if not show_cell_type:
		reorder = domain_reorder
		titles = ["O2", "I1a", "O4", "I1b", "O1", "I2", "I3", "O3", "IS"]

	for i in range(np.unique(reorder).shape[0]):
		#n1 = np.where(domain_reorder==i+1)[0]
		#n1_not = np.where(domain_reorder!=i+1)[0]
		#dr = np.copy(domain_reorder)
		#n1 = np.where(ct_reorder==i+1)[0]
		#n1_not = np.where(ct_reorder!=i+1)[0]
		#dr = np.copy(ct_reorder)
		n1 = np.where(reorder==i+1)[0]
		n1_not = np.where(reorder!=i+1)[0]
		dr = np.copy(reorder)

		print i+1, n1.shape[0]
		dr[n1] = 1
		dr[n1_not] = 0
		xval = mat_new[:,0]
		yval = mat_new[:,1]
		t_mat_1 = mat_new[n1,:]
		t_mat_0 = mat_new[n1_not,:]
		pc1,pc2 = 0,1
		#axn.flat[i].scatter(xval, yval, c=dr, s=5)
		#axn.flat[i].text(np.max(xval), np.max(yval), "D%d" % (i+1))
		if t_mat_1.shape[0]<=2:
			axn.flat[i].scatter(t_mat_0[:,pc1], t_mat_0[:,pc2], c="black", s=5)
			axn.flat[i].scatter(t_mat_1[:,pc1], t_mat_1[:,pc2], c="red", s=5)
		else:
			axn.flat[i] = sns.kdeplot(t_mat_0[:,pc1], t_mat_0[:,pc2], shade=True, ax=axn.flat[i], cmap="Blues", alpha=0.5, shade_lowest=False)
			axn.flat[i] = sns.kdeplot(t_mat_1[:,pc1], t_mat_1[:,pc2], shade=True, ax=axn.flat[i], cmap="Reds", alpha=0.5, shade_lowest=False)
		axn.flat[i].text(np.max(t_mat_0[:,pc1]), np.max(t_mat_0[:,pc2]), titles[i])
	plt.show()
	'''
