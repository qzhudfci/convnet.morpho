#!/usr/bin/python
import math
import sys
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import svm
from sklearn.metrics import confusion_matrix, adjusted_rand_score
sys.setrecursionlimit(10000)
import scanpy as sc
import load_brain2_new
import load_brain1_new
import argparse

font = {"family": "Liberation Sans"}
matplotlib.rc("font", **font)

def shuffle(df, n=1, axis=0):
	df = df.copy()
	for _ in range(n):
		df.apply(np.random.shuffle, axis=axis)
	return df

if __name__=="__main__":
	parser = argparse.ArgumentParser(description="check_unsupervised.", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("-b", "--brain", dest="brain", type=int, required=True, help="Brain ID 1/2")
	parser.add_argument("-o", "--option", dest="option", type=int, required=True, help="Version of dataset to use. Brain 1: 1-high contrast, 2-low contrast, 3-medium contrast; Brain 2: 1-low contrast, 2-high contrast, 3-medium contrast")
	parser.add_argument("-d", "--distance", dest="distance", type=str, required=True, choices=["minkowski", "cosine", "manhattan"])
	parser.add_argument("-a", "--use-current", dest="use_current", action="store_true", help="use a binary")
	parser.add_argument("-ap", "--current-prefix", dest="prefix", default="adata", type=str, help="prefix of binary file")

	args = parser.parse_args()
	t_brain = args.brain
	t_option = args.option
	t_distance = args.distance

	print("==========Setting=========")
	print("t_brain", t_brain)
	print("t_option", t_option)
	print("t_distance", t_distance)
	print("==========================")

	#brain2 setting: option=2, minkowski, n_neighbors=50, random_state=None
	#brain1 setting: option=1, minkowski, n_neighbors=50, random_state=None
	if t_brain==1:
		mat2, ct_reorder, ct2_reorder, domain_reorder, Xcell_reorder, num_cell = load_brain1_new.load(filter_feature=False, log=False, option=t_option)
	else:
		mat2, ct_reorder, ct2_reorder, domain_reorder, Xcell_reorder, num_cell = load_brain2_new.load(filter_feature=False, log=False, option=t_option)

	binary_file = None
	adata = None

	#nissl feature aggregations	
	px_int = pd.read_csv("mat3_2.leiden.2.5.csv", index_col=0, dtype="int32").to_numpy().flatten()
	t_max = np.max(px_int)

	#dapi feature aggregations
	px_int2 = pd.read_csv("mat3_1.leiden.2.5.csv", index_col=0, dtype="int32").to_numpy().flatten()
	u_max = np.max(px_int2)
	print(t_max, u_max, "Done")
	print(px_int.shape, px_int2.shape, "Done")

	if args.use_current:
		binary_file = "%s.brain.%d.option.%d.distance.%s.feature.agg.h5ad" % (args.prefix, t_brain, t_option, t_distance)
		adata = sc.read_h5ad(binary_file)
		print(adata.obs["leiden_2.5"])

	else:
		#aggregate to feature clusters (t_max+1) + (u_max+1)
		agg_mat = np.empty((mat2.shape[0], (t_max+1) + (u_max+1)), dtype="float32")
		#dapi first
		for i in range(u_max+1):
			t_ids = np.where(px_int2==i)[0]
			Y = np.zeros((px_int2.shape[0]), dtype="int32")
			Y[t_ids] = 1
			X = np.transpose(mat2[:, 0:4096])
			clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, verbose=0, max_iter=10000, tol=1e-4, random_state=100)
			clf.fit(X, Y)
			coef = clf.coef_
			#z = coef
			z = 1/(1+np.exp(-coef))
			print(i, "Done")
			#agg_mat[:,i] = np.sum(mat2[:, t_ids], axis=1)
			agg_mat[:,i] = z
		#nissl next
		for i in range(t_max+1):
			t_ids = np.where(px_int==i)[0]
			Y = np.zeros((px_int.shape[0]), dtype="int32")
			Y[t_ids] = 1
			X = np.transpose(mat2[:, 4096:])
			clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, verbose=0, max_iter=10000, tol=1e-4, random_state=100)
			clf.fit(X, Y)
			coef = clf.coef_
			#z = coef
			z = 1/(1+np.exp(-coef))
			print(i, "Done")
			agg_mat[:,(u_max+1)+i] = z
			#agg_mat[:,(u_max+1)+i] = np.sum(mat2[:, 4096+t_ids], axis=1)
		mat2 = agg_mat	
		
		adata = sc.AnnData(X=mat2)
		print(adata)
		sc.pp.neighbors(adata, use_rep='X', metric=t_distance, n_neighbors=50, random_state=100)
		#sc.pp.neighbors(adata, use_rep='X', metric="cosine", n_neighbors=50, random_state=None)
		sc.tl.umap(adata, min_dist=0.1, random_state=100)
		sc.tl.leiden(adata, resolution=1.0, key_added = "leiden_1.0")
		sc.tl.leiden(adata, resolution=1.4, key_added = "leiden_1.4")
		sc.tl.leiden(adata, resolution=2.0, key_added = "leiden_2.0")
		sc.tl.leiden(adata, resolution=2.5, key_added = "leiden_2.5")
		file_prefix="umap.seqfish.brain.%d" % t_brain
		adata.obsm.to_df()[["X_umap1", "X_umap2"]].to_csv("%s_umap.csv" % file_prefix)
		adata.write("%s.brain.%d.option.%d.distance.%s.feature.agg.h5ad" % ("adata", t_brain, t_option, t_distance) , compression="gzip")


	#sc.pl.umap(adata, color=['leiden_1.0','leiden_1.4', 'leiden_2.0', "leiden_2.5"])
	fig = sc.pl.umap(adata, color=['leiden_1.4'], return_fig=True)
	fig.tight_layout()
	fig.savefig("brain.%d.feature.agg.fig1.leiden.1.4.png" % t_brain, dpi=300)

	fig = sc.pl.umap(adata, color=['leiden_2.0'], return_fig=True)
	fig.tight_layout()
	fig.savefig("brain.%d.feature.agg.fig1.leiden.2.0.png" % t_brain, dpi=300)

	'''
	adata.obsm["X_umap"][:, 0:2].to_csv("%s.coord.csv" % file_prefix)
	adata.obs["leiden_1.0"].to_csv("%s.leiden.1.0.csv" % file_prefix)
	adata.obs["leiden_1.4"].to_csv("%s.leiden.1.4.csv" % file_prefix)
	adata.obs["leiden_2.0"].to_csv("%s.leiden.2.0.csv" % file_prefix)
	adata.obs["leiden_2.5"].to_csv("%s.leiden.2.5.csv" % file_prefix)
	'''

	#print(adata.obs["leiden_2.0"].values.astype(int))
	#conf1 = confusion_matrix(domain_reorder, adata.obs["leiden_1.4"].values.astype(int))

	#=============================
	do_item = "domain"
	#=============================

	conf1 = None
	if do_item=="cell_type":
		conf1 = confusion_matrix(ct_reorder, adata.obs["leiden_2.0"].values.astype(int))
	else:
		conf1 = confusion_matrix(domain_reorder, adata.obs["leiden_2.0"].values.astype(int))
		
	print(conf1)

	rsum = np.sum(conf1, axis=1)
	csum = np.sum(conf1, axis=0)
	conf1_norm = np.empty((conf1.shape[0], conf1.shape[1]), dtype="float32")
	for i in range(conf1.shape[0]):
		for j in range(conf1.shape[1]):
			#print(i,j, conf1[i,j], rsum[i], csum[j])
			if rsum[i]==0 or csum[j]==0:
				conf1_norm[i,j] = 0
				continue
			conf1_norm[i,j] = float(conf1[i,j]) / math.sqrt(rsum[i] * csum[j])
	conf1_norm = conf1_norm[~np.all(conf1_norm==0, axis=1)]
	idx = np.argwhere(np.all(conf1_norm[...,:]==0, axis=0))
	conf1_norm = np.delete(conf1_norm, idx, axis=1)
	print(conf1_norm)


	if do_item=="domain" and t_brain==1:
		clust_annot = {1:"L5", 2:"L4", 3:"L2/3", 4:"L1.u,L6b.u", 5:"L1.l,L6b.l", 6:"L6b", 7:"L6a", 8:"L5,L6a", 9:"L4.2"}
		rev_clust_annot = {}
		for ca in clust_annot:
			rev_clust_annot[clust_annot[ca]] = ca
		clust_order = [rev_clust_annot[x] for x in ["L1.u,L6b.u", "L1.l,L6b.l", "L2/3","L4", "L5","L5,L6a","L6a", "L6b", "L4.2"]]
		nj_union = [clust_annot[i] for i in clust_order]
		nj_union_title = []
		for n in nj_union:
			nj_union_title.append(n)
		nt = {}
		#for ki in [4, 16, 1, 9, 8, 13, 3, 11, 12, 2, 5, 7, 10, 0, 6, 15, 14, 16, 17, 18]:
		for ki in [1, 9, 7, 6, 3, 4, 13, 2, 10, 15, 5, 17, 0, 11, 14, 16, 8, 12, 18]:
			nx = [conf1_norm[ai-1,ki] for ai in clust_order]
			nt[ki] = pd.Series(nx, index=nj_union_title)

	elif do_item=="domain" and t_brain==2:
		clust_annot = {1:"O2", 2:"I1a", 3:"O4", 4:"I1b", 5:"O1", 6:"I2", 7:"I3", 8:"O3", 9:"IS"}
		rev_clust_annot = {}
		for ca in clust_annot:
			rev_clust_annot[clust_annot[ca]] = ca
		clust_order = [rev_clust_annot[x] for x in ["O1","O4","O3","O2","I1a","I1b","I2","I3", "IS"]]
		nj_union = [clust_annot[i] for i in clust_order]
		nj_union_title = []
		for n in nj_union:
			nj_union_title.append(n)
		nt = {}
		for ki in [6, 2, 5, 1, 15, 13, 11, 4, 12, 14, 9, 3, 10, 8, 16, 0, 7, 17]:
			nx = [conf1_norm[ai-1,ki] for ai in clust_order]
			nt[ki] = pd.Series(nx, index=nj_union_title)
		
	elif do_item=="cell_type" and t_brain==2:
		clust_annot = {1:"Astro", 2:"Neuron", 3:"Oligo", 4:"Endo", 5:"Micro"}
		rev_clust_annot = {}
		for ca in clust_annot:
			rev_clust_annot[clust_annot[ca]] = ca
		clust_order = [rev_clust_annot[x] for x in ["Endo", "Astro", "Micro", "Oligo", "Neuron"]]
		nj_union = [clust_annot[i] for i in clust_order]
		nj_union_title = []
		for n in nj_union:
			nj_union_title.append(n)
		nt = {}
		#for ki in [13, 14, 9, 7, 10, 15, 4, 5, 0, 6, 12, 2, 8, 11, 1, 3, 12]:
		for ki in [7, 1, 2, 6, 13, 5, 3, 0, 4, 8, 9, 10, 11, 12, 14, 15, 16, 17]:
			nx = [conf1_norm[ai-1,ki] for ai in clust_order]
			nt[ki] = pd.Series(nx, index=nj_union_title)

	elif do_item=="cell_type" and t_brain==1:
		#clust_annot = {1:"Astro", 2:"Endo", 3:"GABA.Neuron", 4:"Glut.Neuron", 5:"Micro", 6:"Oligo.1", 7:"Oligo.2", 8:"Oligo.3"}
		clust_annot = {1:"Astro", 2:"Neuron", 3:"Oligo", 4:"Endo", 5:"Micro"}
		rev_clust_annot = {}
		for ca in clust_annot:
			rev_clust_annot[clust_annot[ca]] = ca
		clust_order = [rev_clust_annot[x] for x in ["Endo", "Astro", "Micro", "Oligo", "Neuron"]]
		nj_union = [clust_annot[i] for i in clust_order]
		nj_union_title = []
		for n in nj_union:
			nj_union_title.append(n)
		nt = {}
		#for ki in [4, 16, 1, 9, 8, 13, 3, 11, 12, 2, 5, 7, 10, 0, 6, 15, 14]:
		for ki in [9, 0, 1, 11, 16, 17, 5, 2, 3, 4, 6, 7, 8, 10, 12, 13, 14, 15, 16, 18]:
			nx = [conf1_norm[ai-1,ki] for ai in clust_order]
			nt[ki] = pd.Series(nx, index=nj_union_title)
		

	matplotlib.rcParams.update({"font.size": 12})

	g = sns.clustermap(pd.DataFrame(nt), row_cluster=False, col_cluster=False, vmax=0.4, figsize=(6,5))
	if do_item=="cell_type":
		g.savefig("brain.%d.feature.agg.fig2.cell.type.png" % t_brain, dpi=300)
	else:
		g.savefig("brain.%d.feature.agg.fig2.domain.png" % t_brain, dpi=300)

	plt.show()



	'''
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)
	ax.set_aspect('equal')
	plt.imshow(conf1_norm, interpolation='nearest', cmap=plt.cm.ocean, vmax=0.4)
	plt.colorbar()
	plt.show()
	'''
	'''
	row_dism = pdist(conf1_norm, metric="correlation")
	row_linkage = hc.linkage(sp.distance.squareform(row_dism), method='single')
	col_dism = pdist(conf1_norm.T, metric="correlation")
	col_linkage = hc.linkage(sp.distance.squareform(col_dism), method='single')
	g = sns.clustermap(conf1_norm, row_linkage=row_linkage, col_linkage=col_linkage, vmax=0.4)
	'''
	plt.show()

	f,axn = plt.subplots(2,5, sharex=True, sharey=True, figsize=(7.5,3))
	plt.subplots_adjust(wspace=0, hspace=0)
	#titles = ['Astro', 'Endo', 'L2/3', 'L4', 'L5', 'L6', 'Micro', 'OPC', 'Oligo', 'Pvalb_1', 'Pvalb_2', 'Sst', 'Vip']
	if do_item=="domain" and t_brain==1:
		reorder = domain_reorder
		titles = ["L5", "L4", "L2/3", "L1.u,L6b.u", "L1.l,L6b.l", "L6b", "L6a", "L5,L6a", "L4.2"]
	elif do_item=="cell_type" and t_brain==1:
		reorder = ct_reorder
		titles = ["Astro", "Neuron", "Oligo", "Endo", "Micro"]
	elif do_item=="cell_type" and t_brain==2:
		reorder = ct_reorder
		titles = ["Astro", "Neuron", "Oligo", "Endo", "Micro"]
	elif do_item=="domain" and t_brain==2:
		reorder = domain_reorder
		titles = ["O2", "I1a", "O4", "I1b", "O1", "I2", "I3", "O3", "IS"]


	is_kdeplot = True
	X_emb = adata.obsm["X_umap"]
	for i in range(np.unique(reorder).shape[0]):
		n1 = np.where(reorder==i+1)[0]
		n1_not = np.where(reorder!=i+1)[0]
		dr = np.copy(reorder)
		print(i+1, n1.shape[0])
		dr[n1] = 1
		dr[n1_not] = 0
		xval = X_emb[:,0]
		yval = X_emb[:,1]
		t_mat_1 = X_emb[n1,:]
		t_mat_0 = X_emb[n1_not,:]
		pc1,pc2 = 0,1
		if t_mat_1.shape[0]<=2:
			axn.flat[i].scatter(t_mat_0[:,pc1], t_mat_0[:,pc2], c="black", s=5)
			axn.flat[i].scatter(t_mat_1[:,pc1], t_mat_1[:,pc2], c="red", s=5)
		elif is_kdeplot:
			axn.flat[i] = sns.kdeplot(t_mat_0[:,pc1], t_mat_0[:,pc2], shade=True, ax=axn.flat[i], cmap="Blues", alpha=0.5, shade_lowest=False)
			axn.flat[i] = sns.kdeplot(t_mat_1[:,pc1], t_mat_1[:,pc2], shade=True, ax=axn.flat[i], cmap="Reds", alpha=0.5, shade_lowest=False)
		else:
			axn.flat[i].scatter(t_mat_0[:,pc1], t_mat_0[:,pc2], c="black", s=5, alpha=0.5)
			axn.flat[i].scatter(t_mat_1[:,pc1], t_mat_1[:,pc2], c="red", s=5, alpha=0.5)
		axn.flat[i].text(np.min(t_mat_0[:,pc1]), np.max(t_mat_0[:,pc2]), titles[i])
		axn.flat[i].set_xticklabels([])
		axn.flat[i].set_yticklabels([])
		axn.flat[i].set_xticks([])
		axn.flat[i].set_xticks([], minor=True)
		axn.flat[i].set_yticks([])
		axn.flat[i].set_yticks([], minor=True)

	if do_item=="domain":
		f.savefig("brain.%d.feature.agg.domain.enrichment.png" % t_brain, dpi=300)
	elif do_item=="cell_type":
		f.savefig("brain.%d.feature.agg.cell.type.enrichment.png" % t_brain, dpi=300)

		#axn.flat[i].text(np.max(t_mat_0[:,pc1]), np.max(t_mat_0[:,pc2]), titles[i])
	plt.show()

	print("Adjusted rand index for leiden 1.4")
	rand_index_ct = adjusted_rand_score(ct_reorder, adata.obs['leiden_1.4'])
	rand_index_domain = adjusted_rand_score(domain_reorder, adata.obs['leiden_1.4'])
	print("cell type", rand_index_ct)
	print("domains", rand_index_domain)

	print("Adjusted rand index for leiden 2.0")
	rand_index_ct = adjusted_rand_score(ct_reorder, adata.obs['leiden_2.0'])
	rand_index_domain = adjusted_rand_score(domain_reorder, adata.obs['leiden_2.0'])
	print("cell type", rand_index_ct)
	print("domains", rand_index_domain)

	sys.exit(0)
