## Tutorial 2: Applying AlexNet deep learning package to extract image based features

### Pre-requisites

- alexnet.pb: Alexnet model
- alexnet.py: Alexnet access functions
- alexnet_frozen.pb: Alexnet model
- bvlc_alexnet.npy: access functions
- caffe_classes.py: access functions
- Download the above files (use the convnet.download_alexnet() below). Also available on [Zenodo](https://zenodo.org/record/5842250).
- tensorflow package from Google. (see Pypi)

### Functions

### download_alexnet(outdir="alexnet")

Parameters:

- outdir: *string*; directory where the alexnet model and access modules will be downloaded to. Default alexnet.

Returns:

- Void

Example:
```
>>> convnet.dowload_alexnet(outdir="alexnet")
```


### alexnet_extract_features(folder, is_visium=False, is_path=True, auto_resize=False, alexnet_path="alexnet")

Parameters:

- folder: *string*; Directory containing the cropped images. 
- is_visium: *boolean*; Visium data or not. Default False.
- is_path: *boolean*; segmented cell images from paths or not. Default True.
- auto_resize: *boolean*; whether or not to resize image if it is too large (greater than 227px). Default False.
- alexnet_path: *string*; Directory where the Alexnet model and access functions are stored. Default alexnet.

Returns:

- Void

Notes:

- Two modes can be chosen from. In the first mode where the data is visium, no attempt is made to segment the cells from images under the spots. Instead the whole tiled images are used as input. In the second mode where data is segmented images, only the segmented cell images will be used as input. Only one mode can be selected.
- The **output** consists of image-features in the `folder/feature` directory. The image feature vector is stored in files of numpy array format (*.npy).
- Alexnet requires the input image to have a uniform dimension of 227px. This function will put the cropped image in the center of a 227px-by-227px square. 


Example:

```
>>> convnet.alexnet_extract_features("outdir.size.150", is_visium=True, is_path=False, auto_resize=False, alexnet_path=".")
# alternate
>>> convnet.alexnet_extract_features("brain1.images/37", is_visium=False, is_path=True, auto_resize=False, alexnet_path=".")
# read a npy file
>>> mat = np.load("brain1.images/37/feature/path34.png.npy")
>>> mat.shape
(1, 4096)
```


