#!/usr/bin/python
import math
import sys
import os
import numpy as np
import scipy
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import random
import sys
import scipy
import scipy.stats
from scipy.interpolate import interp1d
from sklearn.decomposition import PCA
from sklearn.utils.extmath import randomized_svd
import sklearn.datasets
import sklearn.feature_extraction.text
from sklearn.manifold import TSNE
from sklearn.metrics import auc
from sklearn.metrics import adjusted_rand_score, confusion_matrix
from scipy.spatial.distance import pdist
import scipy.spatial as sp, scipy.cluster.hierarchy as hc
from scipy.stats import pearsonr
import os
sys.setrecursionlimit(10000)
from sklearn import svm
from sklearn.svm import SVR
from sklearn.metrics import r2_score
from sklearn.calibration import CalibratedClassifierCV
from sklearn.linear_model import Lasso
from sklearn.linear_model import Ridge
from sklearn.linear_model import ElasticNet
from tensorflow.keras.utils import to_categorical
import scanpy as sc
import load_brain2_new
import load_brain1_new
import load_seqfishplus_new
import reader
import umap
from ivis import Ivis

def shuffle(df, n=1, axis=0):
	df = df.copy()
	for _ in range(n):
		df.apply(np.random.shuffle, axis=axis)
	return df

def do_and_plot_leiden(mat, file_prefix="mat1"):
	adata = sc.AnnData(X=mat)
	sc.pp.neighbors(adata, use_rep='X', metric="cosine", n_neighbors=50, random_state=None)
	print("Done pp neighbors")
	sc.tl.umap(adata, min_dist=0.1, random_state=None)
	print("Done umap")
	sc.tl.leiden(adata, resolution=1.0, key_added = "leiden_1.0")
	print("Done leiden 1.0")
	sc.tl.leiden(adata, resolution=1.4, key_added = "leiden_1.4")
	print("Done leiden 1.4")
	sc.tl.leiden(adata, resolution=2.0, key_added = "leiden_2.0")
	print("Done leiden 2.0")
	sc.tl.leiden(adata, resolution=2.5, key_added = "leiden_2.5")
	print("Done leiden 2.5")
	sc.pl.umap(adata, color=['leiden_1.0','leiden_1.4', 'leiden_2.0', "leiden_2.5"])

	adata.obs["leiden_1.0"].to_csv("%s.leiden.1.0.csv" % file_prefix)
	adata.obs["leiden_1.4"].to_csv("%s.leiden.1.4.csv" % file_prefix)
	adata.obs["leiden_2.0"].to_csv("%s.leiden.2.0.csv" % file_prefix)
	adata.obs["leiden_2.5"].to_csv("%s.leiden.2.5.csv" % file_prefix)

def read_all_png_list():
	id_to_png_1 = read_png_list(brain="1")
	id_to_png_2 = read_png_list(brain="2")
	id_to_png_3 = read_png_list(brain="3")
	all_id_to_png = id_to_png_1 + id_to_png_2 + id_to_png_3
	return all_id_to_png	

def read_png_list(brain="1"):
	file_png_list = ""
	if brain=="1":
		file_png_list = "brain1.segmented/png.file.list.2"
	elif brain=="2":
		file_png_list = "brain2.segmented/png.file.list.2"
	elif brain=="3":
		file_png_list = "seqfishplus/png.file.list.2"
	f = open(file_png_list)
	id_to_png = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		id_to_png.append((ll[0], ll[1]))
	f.close()
	return id_to_png

def read_pair(n1, n2): #_1 which is dapi, _2 which is nissl
	f = open(n1)
	f.readline()
	t_map_1 = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split(",")
		t_map_1.setdefault(int(ll[1]), [])
		t_map_1[int(ll[1])].append(int(ll[0]))
	f.close()
	f = open(n2)
	f.readline()
	t_map_2 = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split(",")
		t_map_2.setdefault(int(ll[1]), [])
		t_map_2[int(ll[1])].append(int(ll[0]))
	f.close()
	return t_map_1, t_map_2
	

if __name__=="__main__":
	b1_mat2, b1_ct_reorder, b1_ct2_reorder, b1_domain_reorder, b1_Xcell_reorder, b1_num_cell = load_brain1_new.load(filter_feature=False, log=False, option=1)
	b2_mat2, b2_ct_reorder, b2_ct2_reorder, b2_domain_reorder, b2_Xcell_reorder, b2_num_cell = load_brain2_new.load(filter_feature=False, log=False, option=1)
	b3_mat2, b3_ct_reorder, b3_domain_reorder, b3_Xcell_reorder, b3_num_cell = load_seqfishplus_new.load(filter_feature=False, log=False, use_equalize=True)

	mat3 = np.empty((b1_num_cell+b2_num_cell+b3_num_cell, 8192), dtype="float32")
	mat3[0:b1_num_cell, :] = b1_mat2
	mat3[b1_num_cell:b1_num_cell+b2_num_cell, :] = b2_mat2
	mat3[b1_num_cell+b2_num_cell:, :] = b3_mat2

	all_id_to_png = read_all_png_list() #dapi, nissl
	t_map_dapi, t_map_nissl = read_pair("mat3_1.leiden.2.5.csv", "mat3_2.leiden.2.5.csv")

	#show montage of all cell images
	for t_id in t_map_dapi:
		ids = t_map_dapi[t_id]
		t_sum = np.sum(mat3[:, ids], axis=1)
		print(t_sum.shape)
		ind = np.sort(np.argsort(-1*t_sum)[:100])
		print("dapi", t_id, ind)
		all_id_to_png_dapi = [x[0] for x in all_id_to_png]
		t_png_list = [all_id_to_png_dapi[i] for i in ind if "image_zero_input" not in all_id_to_png_dapi[i] and "impute" not in all_id_to_png_dapi[i]]
		t_png_list = t_png_list + ["montage_mat3_1_%d.png" % t_id]

		os.system("montage -geometry 100x100\>+2+2 -background '#000000' %s" % (" ".join(t_png_list)))

	for t_id in t_map_nissl:
		ids2 = [x+4096 for x in t_map_nissl[t_id]]
		t_sum = np.sum(mat3[:, ids2], axis=1)
		print(t_sum.shape)
		ind = np.sort(np.argsort(-1*t_sum)[:100])
		print("nissl", t_id, ind)
		all_id_to_png_nissl = [x[1] for x in all_id_to_png]
		t_png_list = [all_id_to_png_nissl[i] for i in ind if "image_zero_input" not in all_id_to_png_nissl[i] and "impute" not in all_id_to_png_nissl[i]]
		t_png_list = t_png_list + ["montage_mat3_2_%d.png" % t_id]

		os.system("montage -geometry 100x100\>+2+2 -background '#000000' %s" % (" ".join(t_png_list)))

	



	sys.exit(0)

	mat3_1 = np.transpose(mat3[:, 0:4096])
	mat3_2 = np.transpose(mat3[:, 4096:])




	print("Doing 0:4096")
	do_and_plot_leiden(mat3_1, file_prefix="mat3_1")
	print("Doing 4096:8192")
	do_and_plot_leiden(mat3_2, file_prefix="mat3_2")

	sys.exit(0)

	mat3 = np.transpose(mat3)
	print(mat3.shape)

	adata = sc.AnnData(X=mat3)
	print(adata)
	sc.pp.neighbors(adata, use_rep='X', metric="cosine", n_neighbors=50, random_state=None)
	print("Done pp neighbors")
	sc.tl.umap(adata, min_dist=0.1, random_state=None)
	print("Done umap")
	sc.tl.leiden(adata, resolution=1.0, key_added = "leiden_1.0")
	print("Done leiden 1.0")
	sc.tl.leiden(adata, resolution=1.4, key_added = "leiden_1.4")
	print("Done leiden 1.4")
	sc.tl.leiden(adata, resolution=2.0, key_added = "leiden_2.0")
	print("Done leiden 2.0")
	sc.tl.leiden(adata, resolution=2.5, key_added = "leiden_2.5")
	print("Done leiden 2.5")
	sc.pl.umap(adata, color=['leiden_1.0','leiden_1.4', 'leiden_2.0', "leiden_2.5"])


	#print(adata.obs["leiden_2.0"].values.astype(int))
	conf1 = confusion_matrix(domain_reorder, adata.obs["leiden_1.4"].values.astype(int))
	#conf1 = confusion_matrix(domain_reorder, adata.obs["leiden_2.0"].values.astype(int))
	print(conf1)

	rsum = np.sum(conf1, axis=1)
	csum = np.sum(conf1, axis=0)
	conf1_norm = np.empty((conf1.shape[0], conf1.shape[1]), dtype="float32")
	for i in range(conf1.shape[0]):
		for j in range(conf1.shape[1]):
			#print(i,j, conf1[i,j], rsum[i], csum[j])
			if rsum[i]==0 or csum[j]==0:
				conf1_norm[i,j] = 0
				continue
			conf1_norm[i,j] = float(conf1[i,j]) / math.sqrt(rsum[i] * csum[j])
	conf1_norm = conf1_norm[~np.all(conf1_norm==0, axis=1)]
	idx = np.argwhere(np.all(conf1_norm[...,:]==0, axis=0))
	conf1_norm = np.delete(conf1_norm, idx, axis=1)
	print(conf1_norm)

	'''
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)
	ax.set_aspect('equal')
	plt.imshow(conf1_norm, interpolation='nearest', cmap=plt.cm.ocean, vmax=0.4)
	plt.colorbar()
	plt.show()
	'''
	row_dism = pdist(conf1_norm, metric="correlation")
	row_linkage = hc.linkage(sp.distance.squareform(row_dism), method='single')
	col_dism = pdist(conf1_norm.T, metric="correlation")
	col_linkage = hc.linkage(sp.distance.squareform(col_dism), method='single')
	g = sns.clustermap(conf1_norm, row_linkage=row_linkage, col_linkage=col_linkage, vmax=0.4)

	plt.show()
	sys.exit(0)

	rand_index_ct = adjusted_rand_score(ct_reorder, adata.obs['leiden_1.4'])
	rand_index_domain = adjusted_rand_score(domain_reorder, adata.obs['leiden_1.4'])

	print(rand_index_ct)
	print(rand_index_domain)
	#X_emb = TSNE(n_components=2, perplexity=10).fit_transform(mat2)
	#X_emb = umap.UMAP(n_neighbors=20, min_dist=0.5).fit_transform(mat2)
	#print(X_emb.shape)

	clust_id = ct_reorder

	original = clust_id.copy()
	#clust_id = clust_id - 1
	#clust_id = to_categorical(clust_id)
	#plt.scatter(X_emb[:,0], X_emb[:,1], s=20, cmap="tab20b", c=clust_id, alpha=1.0)
	#plt.show()

	X = mat2
	Y = clust_id
	clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, \
	verbose=0, max_iter=10000, tol=1e-4)
	clf.fit(X, Y)
	pr = clf.predict(X)
	dec = clf.decision_function(X)
	print(clf.coef_.shape)
	n_sum = np.mean(clf.coef_, axis=0)
	n_greater = np.count_nonzero(clf.coef_ > 0, axis=0)
	#good_ids = np.where((n_greater>=4) & (n_greater<10))[0]
	#good_ids = np.where(n_greater>10)[0]
	good_ids = np.where(n_sum>2e-5)[0]
	#good_ids = np.where(n_sum<0.2e-5)[0]
	#_ = plt.hist(n_sum, bins='auto')
	#plt.show()
	#mat2 = mat2 * n_sum

	#print("good_ids", good_ids.shape)
	#X_emb = TSNE(n_components=2, perplexity=50, metric="minkowski").fit_transform(mat2[:, good_ids])
	#X_emb = TSNE(n_components=2, perplexity=50, metric="minkowski").fit_transform(mat2)
	#X_emb = TSNE(n_components=2, metric="minkowski", learning_rate=10, n_iter=2000).fit_transform(mat2)
	#tfidf = sklearn.feature_extraction.text.TfidfTransformer(norm='l1').fit(mat2)
	#mat2 = tfidf.transform(mat2)
	X_emb = umap.UMAP(n_neighbors=50, min_dist=0.1, metric="minkowski").fit_transform(mat2)
	#X_emb = umap.UMAP(n_neighbors=50, min_dist=0.5).fit_transform(mat2[:,good_ids])
	plt.scatter(X_emb[:,0], X_emb[:,1], s=20, cmap="tab20b", c=ct_reorder, alpha=1.0)
	
	#_ = plt.hist(n_sum, bins='auto')
	#_ = plt.hist(n_greater, bins='auto')
	#print(_)
	plt.show()
	print(n_sum.shape)
	print(dec.shape)

	domain_reorder = np.array(domain_reorder)
	f,axn = plt.subplots(3,5)
	show_cell_type = True
	reorder = ct_reorder
	if not show_cell_type:
		reorder = domain_reorder

	#titles = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"]
	titles = ['Astro', 'Endo', 'L2/3', 'L4', 'L5', 'L6', 'Micro', 'OPC', 'Oligo', 'Pvalb_1', 'Pvalb_2', 'Sst', 'Vip']
	is_kdeplot = True
	for i in range(np.unique(reorder).shape[0]):
		n1 = np.where(reorder==i+1)[0]
		n1_not = np.where(reorder!=i+1)[0]
		dr = np.copy(reorder)
		print(i+1, n1.shape[0])
		dr[n1] = 1
		dr[n1_not] = 0
		xval = X_emb[:,0]
		yval = X_emb[:,1]
		t_mat_1 = X_emb[n1,:]
		t_mat_0 = X_emb[n1_not,:]
		pc1,pc2 = 0,1
		if t_mat_1.shape[0]<=2:
			axn.flat[i].scatter(t_mat_0[:,pc1], t_mat_0[:,pc2], c="black", s=5)
			axn.flat[i].scatter(t_mat_1[:,pc1], t_mat_1[:,pc2], c="red", s=5)
		elif is_kdeplot:
			axn.flat[i] = sns.kdeplot(t_mat_0[:,pc1], t_mat_0[:,pc2], shade=True, ax=axn.flat[i], cmap="Blues", alpha=0.5, shade_lowest=False)
			axn.flat[i] = sns.kdeplot(t_mat_1[:,pc1], t_mat_1[:,pc2], shade=True, ax=axn.flat[i], cmap="Reds", alpha=0.5, shade_lowest=False)
		else:
			axn.flat[i].scatter(t_mat_0[:,pc1], t_mat_0[:,pc2], c="black", s=5, alpha=0.5)
			axn.flat[i].scatter(t_mat_1[:,pc1], t_mat_1[:,pc2], c="red", s=5, alpha=0.5)
		axn.flat[i].text(np.max(t_mat_0[:,pc1]), np.max(t_mat_0[:,pc2]), titles[i])
	plt.show()
	sys.exit(0)




	
	X = mat2
	Y = domain_reorder
	clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, \
	verbose=0, max_iter=10000, tol=1e-4)
	clf.fit(X, Y)
	pr2 = clf.predict(X)
	dec2 = clf.decision_function(X)

	print(clf.coef_.shape)
	print(dec2.shape)
	
	new_dec = np.empty((dec2.shape[0], dec.shape[1]+dec2.shape[1]), dtype="float32")
	new_dec[:, 0:dec.shape[1]] = dec	
	new_dec[:, dec.shape[1]:] = dec2

	X_emb = TSNE(n_components=2, perplexity=50).fit_transform(new_dec)
	#X_emb = umap.UMAP(n_neighbors=20, min_dist=0.5).fit_transform(new_dec)
	plt.scatter(X_emb[:,0], X_emb[:,1], s=20, cmap="tab20b", c=ct_reorder, alpha=1.0)
	plt.show()
	plt.scatter(X_emb[:,0], X_emb[:,1], s=20, cmap="tab20b", c=domain_reorder, alpha=1.0)
	plt.show()

	sys.exit(0)

	#model = Ivis(n_epochs_without_progress=20, ntrees=100, k=15, supervision_weight=0.9)
	#model = Ivis(n_epochs_without_progress=20, supervision_metric="categorical_hinge", supervision_weight=0.8)
	#model.fit(mat2, clust_id)
	#X_emb = model.transform(mat2)
	plt.scatter(X_emb[:,0], X_emb[:,1], s=20, cmap="tab20c", c=original, alpha=1.0)
	plt.show()

	#print(em.shape)
	sys.exit(0)


	nrow = 4
	ncol = 4
	size_factor=10
	f, axn = plt.subplots(nrow, ncol, figsize=(ncol * size_factor, nrow * size_factor))
	plt.subplots_adjust(hspace=0.01, wspace=0.01)
	#cm = plt.cm.get_cmap(colormap)
	ct = 0
	for i in np.unique(clust_id):
		X = np.zeros(clust_id.shape, dtype="int32")
		m = np.where(clust_id==i)[0]
		m2 = np.where(clust_id!=i)[0]
		X[m] = 1
		X[m2] = 0

		X_r = X.copy()
		Xcen = X_emb.copy()
		ind = 0
		
		for j in range(m2.shape[0]):
			X_r[ind] = 0
			#Xcen[ind,:] = [X_emb[m2[j],0], X_emb[m2[j],1]]
			Xcen[ind,:] = [Xcell_reorder[m2[j],0], Xcell_reorder[m2[j],1]]
			ind+=1
		
		for j in range(m.shape[0]):
			X_r[ind] = 1
			#Xcen[ind,:] = [X_emb[m[j],0], X_emb[m[j],1]]
			Xcen[ind,:] = [Xcell_reorder[m[j],0], Xcell_reorder[m[j],1]]
			ind+=1

		max_id = np.max(clust_id) + 1
		axn.flat[ct].scatter(Xcen[:,0], Xcen[:,1], s=10, c=X_r, cmap="tab20", vmin=0, vmax=max_id)
		axn.flat[ct].annotate("cluster %d" % ct, (0.5, 0.95), ha="center")
		axn.flat[ct].title.set_visible(False)
		axn.flat[ct].set_facecolor("white")
		ct+=1
		print(i)

	plt.show()
	sys.exit(0)

	expr_mat = pd.read_csv("../cortex_expression_zscore.csv", sep=",", header=0, index_col=0)

	genes = [g for g in expr_mat.index]

	#print genes	
	expr = {}
	for g in genes:
		expr[g] = expr_mat.loc[g].values
		#np.random.shuffle(expr[g])
		#print expr[g]
		#expr[g] = reader.read_expression("/home/qzhu/Downloads/brain_single/hmrfem.whole.brain.e.fd0-48.correlated.gene/pca_corrected_z_score_cortex/fcortex.gene.%s.txt" % g)
		#expr[g] = expr[g][1:]

		'''
		for i in range(expr[g].shape[0]):
			if expr[g][i]<0:
				expr[g][i] = 0
		'''
		#print expr[g]
	print("Finished reading gene expression.")
	#sys.exit(0)

	'''
	ct_reorder = np.array(ct_reorder)
	ct2_reorder = np.array(ct2_reorder)
	domain_reorder = np.array(domain_reorder)
	'''

	ratio = 0.8 #for training
	show_cell_type = False
	num_class = 9
	domain_reorder = np.array(domain_reorder)
	reorder = domain_reorder
	if show_cell_type:
		#num_class = 8
		num_class = np.unique(ct).shape[0]
		reorder = ct_reorder

	num_times = 1

	#ct_label_map = {'Microglia': 5, 'Combined Oligodendrocyte': 3, \
	#'Endothelial Cell': 4, 'Astrocyte': 1, 'Combined Neuron': 2}

	#genes2 = [g for g in genes]
	#genes2 = reader.read_genes("2.expr.3807genes.pca.output.genes.list") #all the genes
	genes2 = reader.read_genes("genes.top.1000")
	#random.shuffle(genes2)
	num_expr_class = 3
	for ig, g in enumerate(genes2):
		auc_perf, pr_perf = [], []
		n_auc_perf, n_pr_perf = [], []

		early_exit = False

		print("Gene", ig)
		for nt in range(num_times):
			#print "Time", nt
			c_size = []
			train_ex = []
			test_ex = []
			for i in range(num_class):
				n1 = np.where(reorder==i+1)[0]
				num_train = int(n1.shape[0] * ratio)
				c_size.append(num_train)
				this_train_ex = np.random.choice(n1, num_train, replace=False)
				this_test_ex = np.setdiff1d(n1, this_train_ex)
				train_ex.extend(this_train_ex)
				test_ex.extend(this_test_ex)
			train_ex = np.array(train_ex)
			test_ex = np.array(test_ex)

			X = mat2[train_ex, :]
			X_test = mat2[test_ex, :]
			Y = expr[g][train_ex]
			Y_test = expr[g][test_ex]

			tmp_Y = np.zeros(Y.shape, dtype="int32")
			tmp_Y_test = np.zeros(Y_test.shape, dtype="int32")
			for i in range(tmp_Y.shape[0]):
				if Y[i]>0.5:
					tmp_Y[i] = 3
				elif Y[i]<-0.5:
					tmp_Y[i] = 1
				else:
					tmp_Y[i] = 2
			for i in range(tmp_Y_test.shape[0]):
				if Y_test[i]>0.5:
					tmp_Y_test[i] = 3
				elif Y_test[i]<-0.5:
					tmp_Y_test[i] = 1
				else:
					tmp_Y_test[i] = 2

			Y = tmp_Y
			Y_test = tmp_Y_test
			#Y = reorder[train_ex]
			#Y_test = reorder[test_ex]

			#clf = SVR(kernel="poly", C=1e-5)
			#clf = Lasso(alpha=0.1)
			#clf = Ridge(alpha=1.0)

			'''
			clf = ElasticNet(alpha=0.1, l1_ratio=0.5)
			y_lin = clf.fit(X, Y).predict(X_test)
			r2 = r2_score(Y_test, y_lin)
			#r2 = clf.fit(X,Y).score(X_test, Y_test)	
			'''
			#print g, nt, r2
			clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, \
			verbose=0, max_iter=10000, tol=1e-4)
			clf.fit(X, Y)
			pr = clf.predict(X_test)
			dec = clf.decision_function(X_test)

			try:
				r_label = load_seqfishplus_new.get_label_order(pr, dec)
				auc_list, pr_list = load_seqfishplus_new.get_performance_eval(dec, Y_test, r_label)
				auc_perf.append(auc_list)
				pr_perf.append(pr_list)
			except ZeroDivisionError:
				early_exit = True

		if early_exit:
			continue				

		num_ex = len(auc_perf[0][0])
		avg_auc_perf = np.zeros((num_expr_class, num_ex, 2), dtype="float32")
		avg_pr_perf = np.zeros((num_expr_class, num_ex, 2), dtype="float32")
		for ne in range(num_ex):
			for c in range(num_expr_class):
				for ni in range(num_times):
					avg_auc_perf[c, ne, 0] += float(auc_perf[ni][c][ne][0]) / num_times
					avg_auc_perf[c, ne, 1] += float(auc_perf[ni][c][ne][1]) / num_times
					avg_pr_perf[c, ne, 0] += float(pr_perf[ni][c][ne][0]) / num_times
					avg_pr_perf[c, ne, 1] += float(pr_perf[ni][c][ne][1]) / num_times

		for c in range(num_expr_class):
			auc_y = avg_auc_perf[c, :, 0]	
			auc_x = avg_auc_perf[c, :, 1]
			print("Class", g, c+1, auc(auc_x, auc_y))

		#f,axn = plt.subplots(1,num_class)
		for c in range(num_expr_class):
			pr_y = avg_pr_perf[c, :, 0]	
			pr_x = avg_pr_perf[c, :, 1]
			print("Class", g, c+1, "PR", pr_y[5], pr_y[10], pr_y[15], pr_y[20])

		'''
		Y_2, Y_2_test = None, None
		if show_cell_type:
			Y_2 = ct2_reorder[train_ex]
			Y_2_test = ct2_reorder[test_ex]
			
		#print len(train_ex), len(test_ex)
		clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, \
		verbose=0, max_iter=10000, tol=1e-4)
		clf.fit(X, Y)
		pr = clf.predict(X_test)
		dec = clf.decision_function(X_test)

		r_label = get_label_order(pr, dec)
		auc_list, pr_list = get_performance_eval(dec, Y_test, r_label)
		auc_perf.append(auc_list)
		pr_perf.append(pr_list)

		if show_cell_type:
			is_neuron = np.where(Y==ct_label_map["Combined Neuron"])[0]
			clf_neuron = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, \
			verbose=0, max_iter=10000, tol=1e-4)
			X_neuron = X[is_neuron]
			Y_neuron = Y_2[is_neuron]
			clf_neuron.fit(X_neuron, Y_neuron)

			is_neuron_test = np.where(pr==ct_label_map["Combined Neuron"])[0]
			X_test_neuron = X_test[is_neuron_test]
			Y_test_neuron = Y_2_test[is_neuron_test]
			pr_neuron = clf_neuron.predict(X_test_neuron)
			dec_neuron = clf_neuron.decision_function(X_test_neuron)
			#print dec_neuron
			print dec_neuron.shape
			if dec_neuron.ndim==1:
				new_dec_neuron = np.empty((dec_neuron.shape[0], 2), dtype="float32")
				new_dec_neuron[:,0] = dec_neuron
				new_dec_neuron[:,1] = dec_neuron * -1.0
				dec_neuron = new_dec_neuron

			n_r_label = get_label_order(pr_neuron, dec_neuron)
			n_auc_list, n_pr_list = get_performance_eval(dec_neuron, Y_test_neuron, n_r_label)
			n_auc_perf.append(n_auc_list)
			n_pr_perf.append(n_pr_list)

		#all_neuron = np.where(pr==ct_label_map["Combined Neuron"])[0]
		#print all_neuron
		#print dec
		'''

	'''
	num_ex = len(auc_perf[0][0])
	avg_auc_perf = np.zeros((num_class, num_ex, 2), dtype="float32")
	avg_pr_perf = np.zeros((num_class, num_ex, 2), dtype="float32")
	for ne in range(num_ex):
		for c in range(num_class):
			for ni in range(num_times):
				avg_auc_perf[c, ne, 0] += float(auc_perf[ni][c][ne][0]) / num_times
				avg_auc_perf[c, ne, 1] += float(auc_perf[ni][c][ne][1]) / num_times
				avg_pr_perf[c, ne, 0] += float(pr_perf[ni][c][ne][0]) / num_times
				avg_pr_perf[c, ne, 1] += float(pr_perf[ni][c][ne][1]) / num_times

	if show_cell_type:
		num_ex = len(n_auc_perf[0][0])
		neuron_auc_perf = np.zeros((2, num_ex, 2), dtype="float32")
		neuron_pr_perf = np.zeros((2, num_ex, 2), dtype="float32")
		for ne in range(num_ex):
			for ni in range(num_times):
				for c in range(2):
					neuron_auc_perf[c, ne, 0] += float(n_auc_perf[ni][c][ne][0]) / num_times
					neuron_auc_perf[c, ne, 1] += float(n_auc_perf[ni][c][ne][1]) / num_times
					neuron_pr_perf[c, ne, 0] += float(n_pr_perf[ni][c][ne][0]) / num_times
					neuron_pr_perf[c, ne, 1] += float(n_pr_perf[ni][c][ne][1]) / num_times
				
	f,axn = plt.subplots(1,num_class)
	for c in range(num_class):
		auc_y = avg_auc_perf[c, :, 0]	
		auc_x = avg_auc_perf[c, :, 1]
		axn.flat[c].scatter(auc_x, auc_y)
		print "Class", c, auc(auc_x, auc_y)
	plt.show()

	f,axn = plt.subplots(1,num_class)
	for c in range(num_class):
		pr_y = avg_pr_perf[c, :, 0]	
		pr_x = avg_pr_perf[c, :, 1]
		axn.flat[c].scatter(pr_x, pr_y)
		print "Class", c, "PR", pr_y[5], pr_y[10], pr_y[15], pr_y[20]
	plt.show()

	if show_cell_type:
		f,axn = plt.subplots(1,2)
		for c in range(2):
			auc_y = neuron_auc_perf[c, :, 0]	
			auc_x = neuron_auc_perf[c, :, 1]
			axn.flat[c].scatter(auc_x, auc_y)
			print "Class", c, auc(auc_x, auc_y)
		plt.show()

		f,axn = plt.subplots(1,2)
		for c in range(2):
			pr_y = neuron_pr_perf[c, :, 0]	
			pr_x = neuron_pr_perf[c, :, 1]
			axn.flat[c].scatter(pr_x, pr_y)
			print "Class", c, "PR", pr_y[5], pr_y[10], pr_y[15], pr_y[20]
		plt.show()
	'''
	#====================================================

	'''
	num_cor = 0
	cor = {}
	tot = {}
	for ip,truth in zip(pr, Y_test):
		#print ip, truth
		cor.setdefault(truth, 0)
		tot.setdefault(truth, 0)
		tot[truth]+=1
		if ip==truth:
			num_cor+=1
			cor[truth]+=1	
	print num_cor, "out of", len(pr)
	for c in cor:
		print c, cor[c], "out of", tot[c]
	'''

	'''
	pca = PCA(n_components=100)
	pca.fit(mat2)
	mat_new = pca.transform(mat2)
	#mat_new = mat_new[:,1:100]
	print pca.components_.shape
	print pca.explained_variance_ratio_
	print mat_new.shape

	tsne = TSNE(n_components=2)
	mat_new = tsne.fit_transform(mat_new)
	#mat_new = tsne.fit_transform(mat2)

	domain_reorder = np.array(domain_reorder)
	f,axn = plt.subplots(3,3)
	titles = ["Astro", "Endo", "GABA-N", "Glut-N", "Micro", "Oligo.1", "Oligo.2", "Oligo.3"]
	show_cell_type = True

	reorder = ct_reorder
	if not show_cell_type:
		reorder = domain_reorder
		titles = ["O2", "I1a", "O4", "I1b", "O1", "I2", "I3", "O3", "IS"]

	for i in range(np.unique(reorder).shape[0]):
		#n1 = np.where(domain_reorder==i+1)[0]
		#n1_not = np.where(domain_reorder!=i+1)[0]
		#dr = np.copy(domain_reorder)
		#n1 = np.where(ct_reorder==i+1)[0]
		#n1_not = np.where(ct_reorder!=i+1)[0]
		#dr = np.copy(ct_reorder)
		n1 = np.where(reorder==i+1)[0]
		n1_not = np.where(reorder!=i+1)[0]
		dr = np.copy(reorder)

		print i+1, n1.shape[0]
		dr[n1] = 1
		dr[n1_not] = 0
		xval = mat_new[:,0]
		yval = mat_new[:,1]
		t_mat_1 = mat_new[n1,:]
		t_mat_0 = mat_new[n1_not,:]
		pc1,pc2 = 0,1
		#axn.flat[i].scatter(xval, yval, c=dr, s=5)
		#axn.flat[i].text(np.max(xval), np.max(yval), "D%d" % (i+1))
		if t_mat_1.shape[0]<=2:
			axn.flat[i].scatter(t_mat_0[:,pc1], t_mat_0[:,pc2], c="black", s=5)
			axn.flat[i].scatter(t_mat_1[:,pc1], t_mat_1[:,pc2], c="red", s=5)
		else:
			axn.flat[i] = sns.kdeplot(t_mat_0[:,pc1], t_mat_0[:,pc2], shade=True, ax=axn.flat[i], cmap="Blues", alpha=0.5, shade_lowest=False)
			axn.flat[i] = sns.kdeplot(t_mat_1[:,pc1], t_mat_1[:,pc2], shade=True, ax=axn.flat[i], cmap="Reds", alpha=0.5, shade_lowest=False)
		axn.flat[i].text(np.max(t_mat_0[:,pc1]), np.max(t_mat_0[:,pc2]), titles[i])
	plt.show()
	'''
