# Welcome

Convnet.morpho repository stores the code for analyzing cellular morphologies from seqFISH and seqFISH+ datasets.

Note that this repository contains the code only, and does not contain any images.

All the codes in this repository run in Python 3.

## Contents

1. Cell segmentations. Exporting to SVG files. [API](tutorial1.md)
2. [Extracting cell segmentations. Extracting image features associated wth cell images from a convolutional neural network](extract_segmentation.md) [API](tutorial2.md)
3. Clustering image features ([see below](#markdown-header-clustering-image-features)) [API](tutorial3.md)
4. Clustering cells based on image features ([see below](#markdown-header-clustering-cells-based-on-image-features))


## Interested in running a demo?

Separate tutorials are created on running step 1 and [step 2](extract_segmentation.md). We recommend testing these steps separately from steps 3 and 4.
To us, it is more fun to cluster and analyze cell image features anyway. The instructions below use the precomputed output of step1&2 to illustrate steps 3 and 4.

### Datasets

Clone this repository. 

We have 3 datasets: seqFISH Brain1 (1500 cells), seqFISH Brain2 (1500 cells), and seqFISH+ (523 cells).
We have already extracted the image features associated with cell images for all of these cells from the 3 datasets. 
Download the image feature vectors here. 

https://zenodo.org/record/4539873

- Unzip `brain1.image.data.zip` in the directory `brain1.segmented`.
- Unzip `brain2.image.data.zip` in the directory `brain2.segmented`.
- Unzip `seqfishplus.image.data.zip` in the directory `seqfishplus`.


### Tutorials for accessing the APIs

* Tutorial 1: extracting segmentation information from SVG files; then crop images [API](tutorial1.md)
* Tutorial 2: image-feature extraction of deep-learning based features [API](tutorial2.md)
* Tutorial 3: clustering of image features; feature aggregation; clustering of cell images based on image features [API](tutorial3.md)


### Scripts for reproducing figures in the paper

#### Clustering image features

Read [Before proceeding](#markdown-header-before-proceeding).

This command pools cell images from 3 datasets, and clusters AlexNet-extracted image features based on feature values in cells.

```python
python3 final_unsupervised_cluster_features.py
```

- The first plot that is returned is the Dapi feature clusters from clustering the 4096 features of Dapi images. 
- The second plot that is returned is the Nissl feature clusters from clustering the 4096 features of Nissl images. 

#### Clustering cells based on image features

Read [Before proceeding](#markdown-header-before-proceeding).

Seqfish+ dataset:
```python
python3 check_unsupervised_seqfishplus_feature_agg_svm_filtered.py -d minkowski -ap adata
```

Brain1 dataset:
```python
python3 check_unsupervised_seqfish_feature_agg_svm_filtered.py -b 1 -o 1 -d minkowski -ap adata
```

Brain2 dataset:
```python
python3 check_unsupervised_seqfish_feature_agg_svm_filtered.py -b 2 -o 2 -d minkowski -ap adata
```

A set of plots are either generated in the current directory or displayed on screen. These include the morphological cell clustering UMAP and association with gene expression states.

