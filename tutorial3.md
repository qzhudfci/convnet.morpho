## Tutorial 3: Clustering

### Prerequisites

- scanpy package (PyPi)

### Functions

### do_and_plot_leiden(mat, file_prefix="mat1")

Feature clustering for images. AlexNet feature vector is 4096-dimensional. For a feature matrix of cell-by-feature, this performs clustering on the feature dimension using Leiden clustering.

Parameters

- mat: *ndarray*; cell-by-feature matrix. MxN dimension where M is the number of cells, N is the number of features (4096 by default for AlexNet). Feature value is the AlexNet model extracted features based on individual images.
- file_prefix: *string*. Output file prefix

Returns

- adata: *adata*; scanpy adata object containing the clustering results

Notes

- Will perform leiden clustering with default parameters (nearest neighbors = 50, metric = "cosine", random_state = None). This uses the scanpy package to perform leiden clustering.
- Writes the clustering memberships to a *.h5ad file. This can be read using a separate read function.
- 4 Leiden resolutions will be tried and saved (1.0, 1.4, 2.0, 2.5).
- To access the feature clustering results, use `adata.obs["leiden_1.0"]` (for resolution 1.0), `adata.obs["leiden_1.4"]` (for resolution 1.4), etc.

Examples:

```
# get the cell-by-feature matrix in b1_mat2
>>> b1_mat2, b1_ct_reorder, b1_ct2_reorder, b1_domain_reorder, b1_Xcell_reorder, b1_num_cell = load_brain1_new.load(filter_feature=False, log=False, option=1)
>>> mat3 = b1_mat2
# dapi channel (first 4096)
>>> mat3_1 = np.transpose(mat3[:, 0:4096])
# nissl channel (second 4096)
>>> mat3_2 = np.transpose(mat3[:, 4096:])
>>> do_and_plot_leiden(mat3_1, file_prefix="mat3_1")
>>> do_and_plot_leiden(mat3_2, file_prefix="mat3_2")
```

### read_adata(h5ad_file)

Read the adata clustering file (of *.h5ad format).

Parameters:

- h5ad_file: *string*; adata file path (*.h5ad file).

Returns:

- adata: *adata*; With following keys "leiden_1.0", "leiden_1.4", "leiden_2.0", "leiden_2.5"


### feature_aggregation(mat, adata, key="leiden_2.0")

Aggregate features. Convert cell-by-feature matrix into cell-by-aggregated feature matrix.

Parameters:

- mat: *ndarray*; original matrix (cell-by-feature)
- adata: *adata*; scanpy adata object containing groupings of features by Leiden clustering
- key: *string*; key to access the leiden result. Available: leiden_2.0, leiden_1.0, leiden_1.4, leiden_2.5. The number after "_" is the resolution.
- sigmoid: *boolean*; sigmoid transform the result or not.

Returns:

- mat: *ndarray*; newly converted cell-by-aggregated feature matrix.

Note:

- This will use a SVM based approach to aggregate features in the cell matrix. It treats the leiden clusters as gold standard, and seeks to predict the leiden clusters based on original 4096 features. 
- Given original matrix (features by cells) M and weight matrix (cells by aggregated features) W, and the decision matrix Y (features by aggregated features). The value y{i,j} in Y is +1 if feature i belongs to feature cluster j according to Leiden, and -1 otherwise.
- The objective is to find W such that K(M) * W - b = Y, where K() is the kernel function applied on the matrix M. We use a linear kernel. 
- The weight matrix W is returned as result.
- If sigmoid is True, the result matrix is subject to sigmoid transform (1/(1+np.exp(-U))) to make it between 0 and 1.






