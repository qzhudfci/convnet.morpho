#!/usr/bin/python
import math
import sys
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix
sys.setrecursionlimit(10000)
from sklearn import svm
import scanpy as sc
import load_seqfishplus_new
import argparse

font = {"family": "Liberation Sans"}
matplotlib.rc("font", **font)

def shuffle(df, n=1, axis=0):
	df = df.copy()
	for _ in range(n):
		df.apply(np.random.shuffle, axis=axis)
	return df

# seqfishplus
def plot_enrichment(do_item="cell_type", reorder=None, space="physical", Xcell_reorder=None, X_emb=None): 
#space: (physical or morphology_umap)
#Xcell_reorder: for physical space,
#X_emb: for umap space
#do_item: cell_type, or domain, or morphology
#reorder: ct_reorder, domain_reorder, morphology_reorder

	if reorder is None:
		print("Error reorder is None")
		sys.exit(0)

	nrow = 3
	ncol = 5
	if do_item=="cell_type":
		nrow=3
		ncol=5
	elif do_item=="domain":
		nrow=2
		ncol=5
	else:
		nrow=3
		ncol=5

	f,axn = plt.subplots(nrow,ncol, sharex=True, sharey=True, figsize=(1.5*ncol, nrow*1.5))
	plt.subplots_adjust(wspace=0, hspace=0)

	titles = []
	titles_remap = []
	ids_remap = []
	#reorder = None
	if do_item=="cell_type":
		title_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
		titles = ['Astro', 'Endo', 'L2/3 glut', 'L4 glut', 'L5 glut', 'L6 glut', 'Micro', 'OPC', 'Oligo', 'Pvalb_1', 'Pvalb_2', 'Sst', 'Vip']
		title_to_id = dict(zip(titles, title_ids))
		titles_remap = ["Astro", "Endo", "Micro", "OPC", "Oligo", "Pvalb_1", "L2/3 glut", "L4 glut", "Pvalb_2", "L5 glut", "L6 glut", "Vip", "Sst"]
		ids_remap = [title_to_id[x] for x in titles_remap]
	elif do_item=="domain":
		title_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9]
		titles = ["L4", "L5b", "L6_scat", "L6b", "L1", "L5_scat", "L6a", "L5a", "L2/3"]
		title_to_id = dict(zip(titles, title_ids))
		titles_remap = ["L1", "L6_scat", "L6b", "L2/3", "L4", "L5a", "L5b", "L5_scat", "L6a"]
		ids_remap = [title_to_id[x] for x in titles_remap]
	else: #morphology
		reorder_new = np.empty((reorder.shape[0]), dtype="int32")
		for i in range(reorder.shape[0]):
			reorder_new[i] = reorder[i] + 1
		reorder = reorder_new

		title_ids = [(xs+1) for xs in range(np.unique(reorder).shape[0])]
		titles = ["%d" % xs for xs in range(np.unique(reorder).shape[0])]
		title_to_id = dict(zip(titles, title_ids))
		#titles_remap = ["3", "6", "11", "0", "4", "9", "12", "5", "10", "1", "7", "2", "8", "13"]
		titles_remap = ["3", "6", "11", "0", "4", "9", "12", "5", "10", "1", "7", "2", "8"]
		ids_remap = [title_to_id[x] for x in titles_remap]
		#reorder = domain_reorder

	is_kdeplot = True
	bw_adjust = 1
	thresh=0.05
	levels=10
	if do_item=="morphology" and space=="physical":
		bw_adjust = 0.5
		levels=100
		thresh=0.05	

	ind = 0
	for i in ids_remap:
		n1 = np.where(reorder==i)[0]
		n1_not = np.where(reorder!=i)[0]
		dr = np.copy(reorder)
		print(i, n1.shape[0])
		dr[n1] = 1
		dr[n1_not] = 0
		#xval,yval,t_mat_1,t_mat_0=None,None,None,None
		t_mat_1,t_mat_0=None,None

		if space=="physical":
			#xval = Xcell_reorder[:,0]
			#yval = Xcell_reorder[:,1]
			t_mat_1 = Xcell_reorder[n1,:]
			t_mat_0 = Xcell_reorder[n1_not,:]
		elif space=="morphology_umap":
			#xval = X_emb[:,0]
			#yval = X_emb[:,1]
			t_mat_1 = X_emb[n1,:]
			t_mat_0 = X_emb[n1_not,:]
		pc1,pc2 = 0,1
		if t_mat_1.shape[0]<=2:
			axn.flat[ind].scatter(t_mat_0[:,0], t_mat_0[:,1], c="black", s=5)
			axn.flat[ind].scatter(t_mat_1[:,0], t_mat_1[:,1], c="red", s=5)
		elif is_kdeplot:
			axn.flat[ind] = sns.kdeplot(t_mat_0[:,pc1], t_mat_0[:,pc2], shade=True, ax=axn.flat[ind], cmap="Blues", alpha=0.5, shade_lowest=False, bw_adjust=1, levels=10, thresh=0.05)
			axn.flat[ind] = sns.kdeplot(t_mat_1[:,pc1], t_mat_1[:,pc2], shade=True, ax=axn.flat[ind], cmap="Reds", alpha=0.5, shade_lowest=False, bw_adjust=bw_adjust, levels=levels, thresh=thresh)
		else:
			axn.flat[ind].scatter(t_mat_0[:,pc1], t_mat_0[:,pc2], c="black", s=5, alpha=0.5)
			axn.flat[ind].scatter(t_mat_1[:,pc1], t_mat_1[:,pc2], c="red", s=5, alpha=0.5)
		axn.flat[ind].text(np.min(t_mat_0[:,pc1])*1.1, np.max(t_mat_0[:,pc2]), titles_remap[ind])
		axn.flat[ind].set_xticklabels([])
		axn.flat[ind].set_yticklabels([])
		axn.flat[ind].set_xticks([])
		axn.flat[ind].set_xticks([], minor=True)
		axn.flat[ind].set_yticks([])
		axn.flat[ind].set_yticks([], minor=True)
		ind+=1

	if space=="physical":
		if do_item=="cell_type":
			f.savefig("seqfishplus.feature.agg.physical.cell.type.enrichment.png", dpi=300)
		elif do_item=="domain":
			f.savefig("seqfishplus.feature.agg.physical.domain.enrichment.png", dpi=300)
		else:
			f.savefig("seqfishplus.feature.agg.physical.morphology.enrichment.png", dpi=300)
	else:
		if do_item=="cell_type":
			f.savefig("seqfishplus.feature.agg.umap.cell.type.enrichment.png", dpi=300)
		elif do_item=="domain":
			f.savefig("seqfishplus.feature.agg.umap.domain.enrichment.png", dpi=300)
		else:
			f.savefig("seqfishplus.feature.agg.umap.morphology.enrichment.png", dpi=300)
			
	plt.show()
	


if __name__=="__main__":
	parser = argparse.ArgumentParser(description="check_unsupervised.", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	#parser.add_argument("-b", "--brain", dest="brain", type=int, required=True, help="Brain ID 1/2")
	#parser.add_argument("-o", "--option", dest="option", type=int, required=True, help="Version of dataset to use. Brain 1: 1-high contrast, 2-low contrast, 3-medium contrast; Brain 2: 1-low contrast, 2-high contrast, 3-medium contrast")
	parser.add_argument("-u", "--disable-equalize", dest="disable_equalize", action="store_true", help="disable equalize")
	parser.add_argument("-d", "--distance", dest="distance", type=str, required=True, choices=["minkowski", "cosine", "manhattan"])
	parser.add_argument("-a", "--use-current", dest="use_current", action="store_true", help="use a binary")
	parser.add_argument("-ap", "--current-prefix", dest="prefix", default="adata", type=str, help="prefix of binary file")

	args = parser.parse_args()

	t_use_equalize = False
	if args.disable_equalize:
		t_use_equalize = False
	else:
		t_use_equalize = True

	#t_distance = "minkowski"
	t_distance = args.distance

	#===================================
	do_item = "cell_type" #or domain or cell_type
	#===================================

	mat2, ct_reorder, domain_reorder, Xcell_reorder, num_cell = load_seqfishplus_new.load(filter_feature=False, log=False, use_equalize=t_use_equalize)

	f,axn = plt.subplots(1,1,figsize=(5,3))
	plt.subplots_adjust(wspace=0, hspace=0)
	axn.scatter(Xcell_reorder[:,0], Xcell_reorder[:,1], s=15, alpha=1.0)
	axn.set_xticklabels([])
	axn.set_yticklabels([])
	axn.set_xticks([])
	axn.set_xticks([], minor=True)
	axn.set_yticks([])
	axn.set_yticks([], minor=True)
	f.savefig("seqfishplus.coordinates.png", dpi=300)

	#========================================================

	binary_file = None
	adata = None
	#nissl feature aggregations	
	px_int = pd.read_csv("mat3_2.leiden.2.5.csv", index_col=0, dtype="int32").to_numpy().flatten()
	t_max = np.max(px_int)

	#dapi feature aggregations
	px_int2 = pd.read_csv("mat3_1.leiden.2.5.csv", index_col=0, dtype="int32").to_numpy().flatten()
	u_max = np.max(px_int2)
	print(t_max, u_max, "Done")
	if args.use_current:
		binary_file = "%s.brain.seqfishplus.use_equalize.%s.distance.%s.feature.agg.h5ad" % (args.prefix, str(t_use_equalize), t_distance)
		adata = sc.read_h5ad(binary_file)
		print(adata)
	else:
		#aggregate to feature clusters (t_max+1) + (u_max+1)
		agg_mat = np.empty((mat2.shape[0], (t_max+1) + (u_max+1)), dtype="float32")
		#dapi first
		for i in range(u_max+1):
			t_ids = np.where(px_int2==i)[0]
			Y = np.zeros((px_int2.shape[0]), dtype="int32")
			Y[t_ids] = 1
			X = np.transpose(mat2[:, 0:4096])
			clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, verbose=0, max_iter=10000, tol=1e-4, random_state=100)
			clf.fit(X, Y)
			coef = clf.coef_
			#z = coef
			z = 1/(1+np.exp(-coef))
			print(i, "Done")
			#agg_mat[:,i] = np.sum(mat2[:, t_ids], axis=1)
			agg_mat[:,i] = z
		#nissl next
		for i in range(t_max+1):
			t_ids = np.where(px_int==i)[0]
			Y = np.zeros((px_int.shape[0]), dtype="int32")
			Y[t_ids] = 1
			X = np.transpose(mat2[:, 4096:])
			clf = svm.LinearSVC(class_weight="balanced", dual=False, C=1e-5, verbose=0, max_iter=10000, tol=1e-4, random_state=100)
			clf.fit(X, Y)
			coef = clf.coef_
			#z = coef
			z = 1/(1+np.exp(-coef))
			print(i, "Done")
			agg_mat[:,(u_max+1)+i] = z
			#agg_mat[:,(u_max+1)+i] = np.sum(mat2[:, 4096+t_ids], axis=1)
		mat2 = agg_mat	
		adata = sc.AnnData(X=mat2)
		print(adata)
		sc.pp.neighbors(adata, use_rep='X', metric=t_distance, n_neighbors=50, random_state=100)
		sc.tl.umap(adata, min_dist=0.1, random_state=100)
		sc.tl.leiden(adata, resolution=1.0, key_added = "leiden_1.0")
		sc.tl.leiden(adata, resolution=1.4, key_added = "leiden_1.4")
		sc.tl.leiden(adata, resolution=1.6, key_added = "leiden_1.6")
		sc.tl.leiden(adata, resolution=1.8, key_added = "leiden_1.8")
		sc.tl.leiden(adata, resolution=2.0, key_added = "leiden_2.0")
		sc.tl.leiden(adata, resolution=2.5, key_added = "leiden_2.5")

		file_prefix="umap.seqfishplus"
		adata.obsm.to_df()[["X_umap1", "X_umap2"]].to_csv("%s_umap.csv" % file_prefix)
		adata.write("%s.brain.seqfishplus.use_equalize.%s.distance.%s.feature.agg.h5ad" % ("adata", str(t_use_equalize), t_distance) , compression="gzip")

	#sc.pl.umap(adata, color=['leiden_1.0','leiden_1.4', 'leiden_2.0', "leiden_2.5"])
	fig = sc.pl.umap(adata, color=['leiden_2.0'], return_fig = True)
	fig.tight_layout()
	fig.savefig("seqfishplus.feature.agg.fig1.png", dpi=300)

	fig = sc.pl.umap(adata, color=['leiden_1.8'], return_fig = True)
	fig.tight_layout()
	fig.savefig("seqfishplus.feature.agg.fig1_leiden_1.8.png", dpi=300)
	
	print(adata.obs["leiden_2.0"].values.astype(int))

	X_emb = adata.obsm["X_umap"]
	Xcell_reorder2 = Xcell_reorder.copy()
	Xcell_reorder2[:,1] = Xcell_reorder2[:,1]*-1
	adata.obsm["spatial"] = Xcell_reorder2


	fig = sc.pl.embedding(adata, basis="spatial", color=["leiden_2.0"], return_fig=True)
	fig.tight_layout()
	fig.savefig("seqfishplus.feature.agg.fig.spatial.leiden.2.0.png", dpi=300)

	fig = sc.pl.embedding(adata, basis="spatial", color=["leiden_1.8"], return_fig=True)
	fig.tight_layout()
	fig.savefig("seqfishplus.feature.agg.fig.spatial.leiden.1.8.png", dpi=300)

	#plot_enrichment(do_item="cell_type", reorder=ct_reorder, space="physical", Xcell_reorder=Xcell_reorder, X_emb=X_emb)
	#plot_enrichment(do_item="domain", reorder=domain_reorder, space="physical", Xcell_reorder=Xcell_reorder, X_emb=X_emb)
	#plot_enrichment(do_item="morphology", reorder=adata.obs["leiden_2.0"].values.astype(int), space="physical", Xcell_reorder=Xcell_reorder, X_emb=X_emb)


	
	if do_item=="cell_type":
		conf1 = confusion_matrix(ct_reorder, adata.obs["leiden_2.0"].values.astype(int))
	else:
		conf1 = confusion_matrix(domain_reorder, adata.obs["leiden_2.0"].values.astype(int))
	print(conf1.shape) #number of domains x leiden clusters

	rsum = np.sum(conf1, axis=1)
	csum = np.sum(conf1, axis=0)
	conf1_norm = np.empty((conf1.shape[0], conf1.shape[1]), dtype="float32")
	for i in range(conf1.shape[0]):
		for j in range(conf1.shape[1]):
			#print(i,j, conf1[i,j], rsum[i], csum[j])
			if rsum[i]==0 or csum[j]==0:
				conf1_norm[i,j] = 0
				continue
			conf1_norm[i,j] = float(conf1[i,j]) / math.sqrt(rsum[i] * csum[j])
	conf1_norm = conf1_norm[~np.all(conf1_norm==0, axis=1)]
	print(conf1_norm.shape)

	'''
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)
	ax.set_aspect('equal')
	plt.imshow(conf1_norm, interpolation='nearest', cmap=plt.cm.ocean, vmax=0.4)
	plt.colorbar()
	plt.show()
	'''

	if do_item=="domain":
		#plot by domains
		clust_annot = {1:"L4", 2:"L5b", 3:"L6_scat", 4:"L6b", 5:"L1", 6:"L5_scat", 7:"L6a", 8:"L5a", \
		9:"L2/3"}
		rev_clust_annot = {}
		for ca in clust_annot:
			rev_clust_annot[clust_annot[ca]] = ca
		clust_order = [rev_clust_annot[x] for x in ["L6_scat","L1","L6b","L2/3","L4","L5a","L5b","L5_scat",\
		"L6a"]]
		nj_union = [clust_annot[i] for i in clust_order]
		nj_union_title = []
		for n in nj_union:
			nj_union_title.append(n)
		nt = {}
		#for ki in [4, 7, 12, 1, 5, 10, 11, 6, 13, 2, 8, 3, 9, 14]:
		#for ki in [3, 6, 11, 0, 4, 9, 10, 5, 12, 1, 7, 2, 8]:
		for ki in [0, 1, 11, 6, 12, 5, 2, 13, 4, 9, 8, 7, 3, 10, 14, 15]:
			nx = [conf1_norm[ai-1,ki] for ai in clust_order]
			nt[ki] = pd.Series(nx, index=nj_union_title)
	
	else:
		#plot by cell type
		clust_annot = {1:"Astro", 2:"Endo", 3:"L2/3 glut", 4:"L4 glut", 5:"L5 glut", 6:"L6 glut", 7:"Micro", 8:"OPC", \
		9:"Oligo", 10:"Pvalb_1", 11:"Pvalb_2", 12:"Sst", 13:"Vip"}
		rev_clust_annot = {}
		for ca in clust_annot:
			rev_clust_annot[clust_annot[ca]] = ca
		clust_order = [rev_clust_annot[x] for x in ["Astro","Micro","Endo","OPC","Oligo", \
		"Vip", "L6 glut", "L2/3 glut", "L5 glut", \
		"L4 glut","Pvalb_2", "Pvalb_1", "Sst"]]
		nj_union = [clust_annot[i] for i in clust_order]
		nj_union_title = []
		for n in nj_union:
			nj_union_title.append(n)
		nt = {}
		#for ki in [3, 6, 11, 0, 4, 9, 12, 5, 10, 1, 7, 2, 8, 13]:
		#for ki in [3, 6, 11, 0, 4, 9, 12, 5, 10, 1, 7, 2, 8]:
		#for ki in [2, 6, 1, 8, 4, 3, 9, 0, 5, 11, 13, 7, 10, 12, 14]:
		for ki in [0, 1, 6, 7, 12, 11, 3, 2, 5, 8, 4, 9, 14, 13, 10, 15]:
			nx = [conf1_norm[ai-1,ki] for ai in clust_order]
			nt[ki] = pd.Series(nx, index=nj_union_title)
	
	'''
	row_dism = pdist(conf1_norm, metric="correlation")
	row_linkage = hc.linkage(sp.distance.squareform(row_dism), method='single')
	col_dism = pdist(conf1_norm.T, metric="correlation")
	col_linkage = hc.linkage(sp.distance.squareform(col_dism), method='single')
	'''
	#g = sns.clustermap(conf1_norm, row_linkage=row_linkage, col_linkage=col_linkage, vmax=0.4)
	matplotlib.rcParams.update({"font.size": 12})

	g = sns.clustermap(pd.DataFrame(nt), row_cluster=False, col_cluster=False, vmax=0.4, figsize=(6,5))
	if do_item=="cell_type":
		g.savefig("seqfishplus.feature.agg.fig2.cell.type.png", dpi=300)
	else:
		g.savefig("seqfishplus.feature.agg.fig2.domain.png", dpi=300)

	plt.show()

	
	#scatter plot

	plot_enrichment(do_item="cell_type", reorder=ct_reorder, space="morphology_umap", Xcell_reorder=Xcell_reorder, X_emb=X_emb)
	plot_enrichment(do_item="domain", reorder=domain_reorder, space="morphology_umap", Xcell_reorder=Xcell_reorder, X_emb=X_emb)

	
