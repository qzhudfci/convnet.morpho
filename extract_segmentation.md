# Extracting segmentations

Convnet.morpho repository stores the code for analyzing cellular morphologies from seqFISH and seqFISH+ datasets.

This page describes the step 2: extracting segmentation information from interactive cell segmentation in step 1.

We illustrate using 3 datasets.

- Brain1: 1500 cells, 125 genes, ~3000 images (1 dapi, 1 nissl for each cell)
- Brain2: 1500 cells, 125 genes, ~3000 images (1 dapi, 1 nissl for each cell)
- Seqfishplus: 523 cells, 10,000 genes, ~1000 images (1 dapi, 1 nissl for each cell)


## Contents

- Sample results: [Brain1](#markdown-header-brain-1)
- Sample results: [Brain2](#markdown-header-brain-2)
- Sample results: [seqfishplus](#markdown-header-seqfishplus)
- [Utility for extracting cell segmentations](#markdown-header-utility-for-extracting-cell-segmentations-yellow-outlines)
- [Creating one image per cell](#markdown-header-creating-one-image-per-cell)


## Brain 1

Sample result for 1 field of view (FOV):

Input file showing the segmentations in yellow outlines. Left: nissl, Right: dapi.

![](brain1.segmented/demo_pos37_out_resize_50.png)

We extract each yellow outline as a Path object, create a mask based on the path, and put it against a black background. This collapses the whole FOV into a set of cell images (1 image per cell) as seen below.

![](brain1.segmented/demo_pos37_all_nissl_resize_50.png)

Dapi version:

![](brain1.segmented/demo_pos37_all_dapi_resize_50.png)


## Brain 2

Sample result for 1 field of view (FOV):

Input file showing segmentations:

![](brain2.segmented/sample_pos8_out_resize_50.png)

Create cell mask. Replace background with black. Collapsing the FOV into a set of cell images (1 image per cell)

![](brain2.segmented/sample_pos8_all_nissl_resize_50.png)

Dapi version:

![](brain2.segmented/sample_pos8_all_dapi_resize_50.png)


## Seqfishplus

Sample result for 1 field of view (FOV):

Input file showing segmentations:

![](seqfishplus/sample_out_resize_50.png)

Create cell mask. Replace background with black. Collapsing the FOV into a set of cell images (1 image per cell)

![](seqfishplus/sample_0_all_nissl_resize_50.png)

Dapi version:

![](seqfishplus/sample_0_all_dapi_resize_50.png)

## Hands-on

### Utility for extracting cell segmentations (yellow outlines)

Cell segmentations are encoded as path objects in the SVG file saved by the Concepts App.

We wrote a small utility to extract this information:

- Open an editor and edit the file `view_svg.html` from `web.util` directory of repository ROOT. Go to **line 10**:
```html
<object id="svg2" type="image/svg+xml" data="Pos4dap.svg">
```
Where it says `Pos4dap.svg`, change it to the svg file you would like to extract information from. Save the file `view_svg.html` and exit. For the purpose of illustration, we leave this unchanged. The sample svg file is supplied in `web.util` directory.

- Run the following
```bash
cd web.util
python3 -m http.server
```

- Open a browser, and browse to the URL: **localhost:8000/view_svg.html** 

- You will see the following SVG extracted output information:

![](web.util/sample_output_1.png)

**Explanation**: in the text field, each line that begins with "path" is a cell. Path20 is for example the cell ID, and contains the X and Y coordinate of centroid of cell segmentation.

Scroll down the page. You will see the following:

![](web.util/sample_output_2.png)

**Explanation**: each line beginning (0 path20 0 62.34 92.50) now specifies the 200 points that constitute the cell segmentation path for that cell. 

- Copy and paste the entire text of the webpage to a separate file. Save it as "output.XX.txt" where XX is the FOV ID. See example [`output_Pos4.txt`](web.util/output_Pos4.txt).


### Creating one image per cell

With the outlines extracted to output.XX.txt, we can next crop a cell image using the outline.

Inputs needed: 

- [`output_Pos4.txt`](web.util/output_Pos4.txt) (paths from previous step)

- [`Pos4.7.rotate.png`](web.util/Pos4.7.rotate.png) (FOV image)

Output:

- directory `out`

Command file: 

- [`transform.pad.py`](web.util/transform.pad.py) (in web.util directory in repository ROOT)

```bash
cd web.util
mkdir out
python3 transform.pad.py output_Pos4.txt Pos4.7.rotate.png out
```

Expected result:

```bash
ls -ltr out
total 968
-rw-r--r-- 1 qzhu qzhu 10721 Feb 14 08:01 path20.png
-rw-r--r-- 1 qzhu qzhu 10151 Feb 14 08:01 path22.png
-rw-r--r-- 1 qzhu qzhu  8372 Feb 14 08:01 path24.png
-rw-r--r-- 1 qzhu qzhu  7381 Feb 14 08:01 path26.png
-rw-r--r-- 1 qzhu qzhu  3486 Feb 14 08:01 path28.png
-rw-r--r-- 1 qzhu qzhu  9953 Feb 14 08:01 path30.png
-rw-r--r-- 1 qzhu qzhu  8843 Feb 14 08:01 path32.png
-rw-r--r-- 1 qzhu qzhu  6902 Feb 14 08:01 path34.png
-rw-r--r-- 1 qzhu qzhu  8081 Feb 14 08:01 path36.png
-rw-r--r-- 1 qzhu qzhu  9800 Feb 14 08:01 path38.png
...
```



