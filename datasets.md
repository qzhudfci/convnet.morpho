## Dataset API Tutorial: How to access the three morphology datasets

There are three datasets available for immediate use in this package. For convenience we name them **brain1**, **brain2**, and **seqfishplus**. Brain1 has 1401 cells (2802 images), Brain2 1589 cells (3168 images), and SeqFISHplus 523 cells (1046 images). Each cell has two images: DAPI and Nissl. Brain1 and 2 are mouse visual cortex, while seqFISH is the somatosensory cortex.

### Prerequisites


### Sections

*  [Dataset 1 (Brain1) Functions](#markdown-header-dataset-1-functions); **1401 cells; 2802 images; mouse V1**
*  [Dataset 2 (Brain2) Functions](#markdown-header-dataset-2-functions); **1589 cells; 3168 images; mouse V1**
*  [Dataset 3 (seqfishplus) Functions](#markdown-header-dataset-3-functions); **523 cells; 1046 images; mouse SS**


### Functions

### Dataset 1 Functions 

### load_brain1_new.download(outdir="brain1.segmented"):

Download Brain1 dataset. See load_brain1_new.load() for description of the dataset.

Parameters

*  outdir: *string*; directory where it will be downloaded to

Returns

*  Void

Example

```python
>>> load_brain1_new.download(outdir="brain1.segmented")
```


### load_brain1_new.load(filter_feature=False, filter_cutoff=50, log=False, zscore1=False, option=1, prereq_dir="brain1.segmented")

Brain1 dataset access. This is a V1 visual cortex seqFISH dataset with morphology images (dapi, nissl channels), and cellular gene expression vectors for 125 genes assayed by seqFISH. It allows you to access the morphology features for individual cells (4096 dimensional), and cell type identity (discrete label), and spatial domain identify (discrete label) for these cells. 

Parameters

*  filter_feature: *boolean*; whether or not to filter the morphology features
*  filter_cutoff: *float*; if filter_feature=True, the cutoff on the feature value in order to remove a feature (deprecated)
*  log: *boolean*; whether or not to log-transform the feature value
*  zscore1: *boolean*; whether or not to zscore transform the result
*  option: *int*; {1, 2, 3}; Versions of image processing. 1 - high contrast, dark background; 2 - low contrast, light background; 3 - medium contrast, medium background
*  prereq_dir: *string*; where to find the brain1 dataset prerequisite files

Returns

*  mat2, ct_reorder, ct2_reorder, domain_reorder, Xcen_reorder, num_cell
*  mat2: *ndarray*; shape (num cells, 8192) of *float*; AlexNet-extracted image feature values; 8192 is the number of features: first 4096 is the dapi channel features, and second 4096 is the nissl channel features
*  ct_reorder: *ndarray*;  shape (num cells, ) of *int*; Level 1 cell type integer labels for all cells. 
    *  Level 1 integer labels can be chosen from {'Astrocyte': 1, 'Combined Neuron': 2, 'Combined Oligodendrocyte': 3, 'Endothelial Cell': 4, 'Microglia': 5}.
*  ct2_reorder: *ndarray*; shape (num cells, ) of *int*; Level 2 cell type integer labels for all cells. 
    *  Level 2 labels can be chosen from: {'Astrocyte': 1, 'Endothelial Cell': 2, 'GABA-ergic Neuron': 3, 'Glutamatergic Neuron': 4, 'Microglia': 5, 'Oligodendrocyte.1': 6, 'Oligodendrocyte.2': 7, 'Oligodendrocyte.3': 8}.
*  domain_reorder: *ndarray*; shape (num cells, ) of *int*; Spatial domain integer labels for all cells. 
    *  Spatial domain label can be chosen from: {"L5":1, "L4":2, "L2/3":3, "L1/L6b upper":4, "L1/L6b lower":5, "L6b":6, "L6a":7, "L5,L6a":8, "L4.2":9}.
*  Xcen_reorder: *ndarray*; shape (num cells, 2) of *float*; physical coordinates of cells (x-coord, y-coord)


Examples:

```
>>> mat2, ct_reorder, ct2_reorder, domain_reorder, Xcell_reorder, num_cell = load_brain1_new.load(filter_feature=False, log=False, option=1, prereq_dir="brain1.segmented")
>>> mat2.shape
(1401, 8192)
>>> dapi_mat2=mat2[:,0:4096]
>>> nissl_mat2=mat2[:,4096:]
>>>
>>> ct_reorder
array([2, 1, 5, ..., 2, 2, 3])
>>> ct2_reorder
array([3, 1, 5, ..., 4, 3, 7])
>>> Xcell_reorder
array([[-1.00000e+00, -1.00000e+00],
       [ 1.54100e+02, -4.79000e+01],
       [ 2.50130e+02, -8.12300e+01],
       ...,
       [ 7.13160e+02, -2.92285e+03],
       [ 7.83230e+02, -3.22334e+03],
       [ 4.06838e+03, -2.73472e+03]], dtype=float32)
>>> domain_reorder
array([5, 5, 5, ..., 5, 9, 6])
>>>
>>> #interpreting the labels
>>> ct_map = {1: 'Astrocyte', 2: 'Combined Neuron', 3: 'Combined Oligodendrocyte', 4: 'Endothelial Cell', 5: 'Microglia'}
>>> ct2_map = {1: 'Astrocyte', 2: 'Endothelial Cell', 3: 'GABA-ergic Neuron', 4: 'Glutamatergic Neuron', 5: 'Microglia', 6: 'Oligodendrocyte.1', 7: 'Oligodendrocyte.2', 8: 'Oligodendrocyte.3'}
>>> domain_map = {1: "L5", 2: "L4", 3: "L2/3", 4: "L1/L6b upper", 5: "L1/L6b lower", 6: "L6b", 7: "L6a", 8: "L5,L6a", 9: "L4.2"}
>>> [ct_map[c] for c in ct_reorder]
['Combined Neuron', 'Astrocyte', 'Microglia', 'Combined Neuron', 'Combined Neuron', 'Combined Neuron', ...]
>>> [ct2_map[c] for c in ct2_reorder]
['GABA-ergic Neuron', 'Astrocyte', 'Microglia', 'GABA-ergic Neuron', 'GABA-ergic Neuron', 'Glutamatergic Neuron', ...]
>>> [domain_map[c] for c in domain_reorder]
['L1/L6b lower', 'L1/L6b lower', 'L1/L6b lower', 'L1/L6b upper', 'L1/L6b upper', 'L1/L6b upper', 'L1/L6b upper', 'L1/L6b lower', ...]
>>> Xcell_reorder.shape
(1401, 2)
>>> num_cell
1401
```

### Dataset 2 Functions

### load_brain2_new.download(outdir="brain2.segmented"):

Download Brain2 dataset. See load_brain2_new.load() for description of the dataset.

Parameters

*  outdir: *string*; directory where it will be downloaded to

Returns

*  Void

Example

```python
>>> load_brain2_new.download(outdir="brain2.segmented")
```


### load_brain2_new.load(filter_feature=False, filter_cutoff=50, log=False, zscore1=False, option=2, prereq_dir="brain2.segmented")

Brain2 dataset access. This is a V1 visual cortex (**replicate 2**) seqFISH dataset with morphology images (dapi, nissl channels), and cellular gene expression vectors for 125 genes assayed by seqFISH. It allows you to access the morphology features for individual cells (4096 dimensional), and cell type identity (discrete label), and spatial domain identify (discrete label) for these cells. 

Parameters

*  filter_feature: *boolean*; whether or not to filter the morphology features
*  filter_cutoff: *float*; if filter_feature=True, the cutoff on the feature value in order to remove a feature (deprecated)
*  log: *boolean*; whether or not to log-transform the feature value
*  zscore1: *boolean*; whether or not to zscore transform the result
*  option: *int*; {1, 2, 3}; Versions of image processing. 1 - low contrast, light background; 2 - high contrast, dark background; 3 - medium contrast, medium background
*  prereq_dir: *string*; where to find the brain2 dataset prerequisite files

Returns

*  mat2, ct_reorder, ct2_reorder, domain_reorder, Xcen_reorder, num_cell
*  mat2: *ndarray*; shape (num cells, 8192) of *float*; AlexNet-extracted image feature values; 8192 is the number of features: first 4096 is the dapi channel features, and second 4096 is the nissl channel features
*  ct_reorder: *ndarray*;  shape (num cells, ) of *int*; Level 1 cell type integer labels for all cells. 
    *  Level 1 integer labels can be chosen from {'Astrocyte': 1, 'Combined Neuron': 2, 'Combined Oligodendrocyte': 3, 'Endothelial Cell': 4, 'Microglia': 5}.
*  ct2_reorder: *ndarray*; shape (num cells, ) of *int*; Level 2 cell type integer labels for all cells. 
    *  Level 2 labels can be chosen from: {'Astrocyte': 1, 'Endothelial Cell': 2, 'GABA-ergic Neuron': 3, 'Glutamatergic Neuron': 4, 'Microglia': 5, 'Oligodendrocyte.1': 6, 'Oligodendrocyte.2': 7, 'Oligodendrocyte.3': 8}.
*  domain_reorder: *ndarray*; shape (num cells, ) of *int*; Spatial domain integer labels for all cells. 
    *  Spatial domain label can be chosen from: {"O2":1, "I1a":2, "O4":3, "I1b":4, "O1":5, "I2":6, "I3":7, "O3":8, "IS":9}.
*  Xcen_reorder: *ndarray*; shape (num cells, 2) of *float*; physical coordinates of cells (x-coord, y-coord)

Examples:

```
>>> mat2, ct_reorder, ct2_reorder, domain_reorder, Xcell_reorder, num_cell = load_brain2_new.load(filter_feature=False, log=False, option=2, prereq_dir="brain2.segmented")
>>> mat2.shape
(1589, 8192)
>>> dapi_mat2=mat2[:,0:4096]
>>> nissl_mat2=mat2[:,4096:]
>>>
>>> ct_reorder
array([2, 2, 2, ..., 2, 2, 2])
>>> ct2_reorder
array([4, 4, 4, ..., 4, 3, 3])
>>> Xcell_reorder
array([[  265.76,  -231.14],
       [  290.48,  -261.52],
       [  257.12,  -133.35],
       ...,
       [ 1388.76, -1880.47],
       [ 5172.85, -1340.96],
       [ 5220.6 , -1523.37]], dtype=float32)
>>> domain_reorder
array([5, 5, 5, ..., 4, 3, 8])
>>>
>>> #interpreting the labels
>>> ct_map = {1: 'Astrocyte', 2: 'Combined Neuron', 3: 'Combined Oligodendrocyte', 4: 'Endothelial Cell', 5: 'Microglia'}
>>> ct2_map = {1: 'Astrocyte', 2: 'Endothelial Cell', 3: 'GABA-ergic Neuron', 4: 'Glutamatergic Neuron', 5: 'Microglia', 6: 'Oligodendrocyte.1', 7: 'Oligodendrocyte.2', 8: 'Oligodendrocyte.3'}
>>> domain_map = {1: "O2", 2: "I1a", 3: "O4", 4: "I1b", 5: "O1", 6: "I2", 7: "I3", 8: "O3", 9: "IS"}
>>> [ct_map[c] for c in ct_reorder]
['Combined Neuron', 'Combined Neuron', 'Combined Neuron', 'Combined Neuron', 'Combined Neuron', 'Combined Neuron', ...]
>>> [ct2_map[c] for c in ct2_reorder]
['Glutamatergic Neuron', 'Glutamatergic Neuron', 'Glutamatergic Neuron', 'Glutamatergic Neuron', 'Glutamatergic Neuron', ...]
>>> [domain_map[c] for c in domain_reorder]
['O1', 'O1', 'O1', 'O1', 'O1', 'O1', 'O1', 'O1', 'O1', 'O1', 'O1', 'O1', 'O1', 'O1', 'O1', 'O1', 'O1',...]
>>> Xcell_reorder.shape
(1589, 2)
>>> num_cell
1589
```


### Dataset 3 Functions

### load_seqfishplus_new.download(outdir="seqfishplus.segmented"):

Download seqfishplus dataset. See load_seqfishplus_new.load() for description of the dataset.

Parameters

*  outdir: *string*; directory where it will be downloaded to

Returns

*  Void

Example

```python
>>> load_seqfishplus_new.download(outdir="seqfishplus.segmented")
```


### load_seqfishplus_new.load(filter_feature=False, filter_cutoff=50, log=False, is_shape=False, use_equalize=True, is_texture=False, prereq_dir="seqfishplus.segmented")

Seqfishplus dataset access. This is a SS somatosensory cortex seqFISHplus dataset with morphology images (dapi, nissl channels), and cellular gene expression vectors for 10K genes assayed by seqFISHplus. It allows you to access the morphology features for individual cells (4096 dimensional), and cell type identity (discrete label), and spatial domain identify (discrete label) for these cells. 

Parameters

*  filter_feature: *boolean*; whether or not to filter the morphology features
*  filter_cutoff: *float*; if filter_feature=True, the cutoff on the feature value in order to remove a feature (deprecated)
*  log: *boolean*; whether or not to log-transform the feature value
*  is_shape: *boolean*; whether or not to use cell shape alone and nothing else (see notes)
*  use_equalize: *boolean*; whether or not to use equalization on images (see notes)
*  is_texture: *boolean*; whether or not to use cell texture and forget about cell shape (see notes)
*  prereq_dir: *string*; where to find the seqfishplus dataset prerequisite files

Returns

*  mat2, ct_reorder, domain_reorder, Xcen_reorder, num_cell
*  mat2: *ndarray*; shape (num cells, 8192) of *float*; AlexNet-extracted image feature values; 8192 is the number of features: first 4096 is the dapi channel features, and second 4096 is the nissl channel features
*  ct_reorder: *ndarray*;  shape (num cells, ) of *int*; Cell type integer labels for all cells. 
    *  Cell type integer labels can be chosen from {1:"Astro", 2:"Endo", 3:"L2/3 glut", 4:"L4 glut", 5:"L5 glut", 6:"L6 glut", 7:"Micro", 8:"OPC", 9:"Oligo", 10:"Pvalb_1", 11:"Pvalb_2", 12:"Sst", 13:"Vip"}.
*  domain_reorder: *ndarray*; shape (num cells, ) of *int*; Spatial domain integer labels for all cells. 
    *  Spatial domain label can be chosen from: {1:"L4", 2:"L5b", 3:"L6_scat", 4:"L6b", 5:"L1", 6:"L5_scat", 7:"L6a", 8:"L5a", 9:"L2/3"}.
*  Xcen_reorder: *ndarray*; shape (num cells, 2) of *float*; physical coordinates of cells (x-coord, y-coord)

Notes

*  When outputing Alexnet-extracted features, four versions of morphology images are available to choose. 
    *  If use_equalize=True (and is_shape and is_texture False), use histogram-equalized cell images. 
    *  If is_shape=True (and use_equalize and is_texture False), use cell shape white-masks as images. 
    *  If is_texture=True (use_equalize and is_shape False), use a circle around cell centroid as cell mask and fill the circle with cell pixels as cell images (in this way forget about cell shape). 
    *  If use_equalize, is_texture, is_shape are all False, no equalization or image manipulation is made.


Examples:

```
>>> mat2, ct_reorder, domain_reorder, Xcell_reorder, num_cell = load_seqfishplus_new.load(filter_feature=False, log=False, use_equalize=True, prereq_dir="seqfishplus.segmented")
>>> mat2.shape
(523, 8192)
>>> dapi_mat2=mat2[:,0:4096]
>>> nissl_mat2=mat2[:,4096:]
>>> nissl_mat2
array([[1.8849995 , 0.        , 0.48587418, ..., 0.        , 0.        ,
        0.        ],
       [0.        , 0.        , 0.        , ..., 0.        , 0.        ,
        0.        ],
       [4.6877956 , 0.        , 0.        , ..., 0.        , 0.        ,
        0.        ],
       ...,
       [2.4720538 , 0.        , 0.88054216, ..., 0.        , 0.        ,
        0.        ],
       [4.025478  , 0.        , 0.        , ..., 0.        , 0.        ,
        0.        ],
       [0.        , 0.        , 0.        , ..., 0.        , 0.        ,
        0.        ]], dtype=float32)
>>> ct_reorder
array([2, 2, 2, ..., 2, 2, 2])
>>> ct2_reorder
array([ 3,  3,  3, 10,  3, 10,  3, 13,  3,  3,  3,  3,  7,  3,  3, 10,  3,
        3,  7,  3,  3, 10,  2, 10,  3, 13,  3,  3,  3,  3,  3,  3,  3,  3,
...])
>>> Xcell_reorder
array([[ 1632. , -2943.7],
       [ 1589.5, -2307.5],
       [ 1539.9, -2823.9],
       ...,
       [ 6231. ,  -297. ],
       [ 6220.5,  -456.6],
       [ 6198.7, -1055.6]], dtype=float32)
>>> domain_reorder
array([9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 5, 9, 9, 9,
       5, 5, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 5, 9, 5, 4, 5, 5, 5, 5, 5, 5,
       5, 4, 9, 5, 6, 5, 1, 9, 9, 9, 1, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
...])
>>>
>>> #interpreting the labels
>>> ct_map = {1:"Astro", 2:"Endo", 3:"L2/3 glut", 4:"L4 glut", 5:"L5 glut", 6:"L6 glut", 7:"Micro", 8:"OPC", 9:"Oligo", 10:"Pvalb_1", 11:"Pvalb_2", 12:"Sst", 13:"Vip"}
>>> domain_map = {1:"L4", 2:"L5b", 3:"L6_scat", 4:"L6b", 5:"L1", 6:"L5_scat", 7:"L6a", 8:"L5a", 9:"L2/3"}
>>> [ct_map[c] for c in ct_reorder]
['L2/3 glut', 'L2/3 glut', 'L2/3 glut', 'Pvalb_1', 'L2/3 glut', 'Pvalb_1', 'L2/3 glut', 'Vip', 'L2/3 glut', 'L2/3 glut',...]
>>> [domain_map[c] for c in domain_reorder]
['L2/3', 'L2/3', 'L2/3', 'L2/3', 'L2/3', 'L2/3', 'L2/3', 'L2/3', 'L2/3', 'L2/3', 'L2/3', 'L2/3', 'L2/3', 'L2/3', ...]
>>> Xcell_reorder.shape
(523, 2)
>>> num_cell
523
```
